function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    // call default implementation to actually install the executable!
    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addOperation(
            "CreateShortcut",
            "@TargetDir@/fe-preview.exe",
            "@StartMenuDir@/Fattura Elettronica Preview.lnk",
            "workingDirectory=@TargetDir@",
            "iconPath=@TargetDir@/fe-preview.ico",
            "description=Launch Fattura Elettronica Preview");
    }
}
