# Fattura Elettronica Preview

Viewer for the Fattura Elettronica format.

Fattura Elettronica is an XML file format used in Italy to represent invoices.
fe-preview displays this file format in human readable form.

This tool does not have editing capabilities and the rendering is done thanks
to various style-sheets available online from third sources.

More technically, this is simply a Qt WebEngineView where the XML file is
loaded with an XSL stylesheet applied. The XML file is then automatically
rendered as HTML page.

Features:

- display XML and P7M invoices in "Fattura elettronica" and "Fattura
    elettronica PA" format

- display SdI messages and metadata files

- parse the attachments to allow the display or download

- export tool with a file name containing metadata extracted from the invoice
    (company name, invoice number etc). The exported file can be:
    - the same XML or P7M file "saved as" or renamed
    - a PDF in the human redable form

- add any XSL stylesheets and switch between them on the fly

- tool to download automatically the three most used stylesheets

- extract P7M format using the embedded OpenSSL library or an external OpenSSL
    executable

## Contacts

Git repository:

https://gitlab.com/ariacorrente/fattura-elettronica-preview
