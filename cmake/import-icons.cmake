#
# IMPORT-ICONS
#
# cmake script for importing the fallback icons from a theme.
#
# The use case of this script is the developer wanting to add new icons to the
# source code, NOT the user compiling the program. The required fallback icons
# are already present in the repository, no need to import them at build time.
#
# This script is meant to be used with the Breeze icon theme:
# https://develop.kde.org/frameworks/breeze-icons/
# but should work with any icon theme following the freedesktop standard and
# having the required icons in SVG format.
#
# This script is used to update the icon resources in the source folder. When
# a new icon is added to the program the procedure to follow is:
# 1- download the Breeze icon theme
# 2- unzip/compile the theme in a folder <path-to-icon-theme->
# 3- configure the build system setting the folder where to search for the icons
#    in the variable ICON_THEME_DIR, something like:
#       cmake -DICON_THEME_DIR=<path-to-icon-theme->
# 4- add the new icon name to the variable ICON_NAMES in this script
# 5- run this script calling the target "import_icons":
#       make import_icons
#
# From cmake documentation:
# When run in -P script mode, CMake sets the variables CMAKE_BINARY_DIR,
# CMAKE_SOURCE_DIR, CMAKE_CURRENT_BINARY_DIR and CMAKE_CURRENT_SOURCE_DIR to
# the current working directory.

# FILE GLOB_RECURSE calls should not follow symlinks by default.
cmake_policy(SET CMP0009 NEW)

set(RCC_FILE_NAME fallback-icons.qrc)
set(THEME_NAME Breeze)
set(ICON_NAMES
    document-open
    configure
    view-refresh
    document-save-as
    application-pdf
    help-about
    preferences-other
)

if(NOT ICON_THEME_DIR)
    message(FATAL_ERROR "ICON_THEME_DIR must point to the root folder of the icon theme to import.")
endif(NOT ICON_THEME_DIR)

message(STATUS "Searching icons in '${ICON_THEME_DIR}'...")

foreach(ICON_NAME IN LISTS ICON_NAMES)

    # Search for the icons
    file(GLOB_RECURSE ICON_PATHS
        RELATIVE ${ICON_THEME_DIR}
        ${ICON_THEME_DIR}/*/${ICON_NAME}.svg)
    set(IS_ICON_FOUND FALSE)

    # Copy the icons and compose the content of the Qt resource file
    foreach(ICON_PATH IN LISTS ICON_PATHS)
        get_filename_component(ICON_DIR  ${ICON_PATH} DIRECTORY)
        get_filename_component(ICON_SIZE ${ICON_DIR} NAME)
        get_filename_component(ICON_NAME ${ICON_PATH} NAME_WLE)
        set(FULL_SRC_PATH ${ICON_THEME_DIR}/${ICON_PATH})

        if(ICON_SIZE EQUAL "32" AND NOT IS_SYMLINK ${FULL_SRC_PATH})
            file(INSTALL ${FULL_SRC_PATH}
                DESTINATION ${CMAKE_SOURCE_DIR}/resources/icons/${THEME_NAME}/${ICON_DIR})
            set(FILE_TAGS "${FILE_TAGS}\n        <file alias=\"${ICON_NAME}\">icons/${THEME_NAME}/${ICON_PATH}</file>")
            set(IS_ICON_FOUND TRUE)
            # Stop at first icon found to avoid doubles
            break()
        endif(ICON_SIZE EQUAL "32" AND NOT IS_SYMLINK ${FULL_SRC_PATH})
    endforeach()

    # Tell the developer if we had success
    if(NOT IS_ICON_FOUND)
        message(WARNING "Fallback icon for '${ICON_NAME}' NOT found")
    else()
        message(STATUS "Fallback icons for ${ICON_NAME} found.")
    endif(NOT IS_ICON_FOUND)

endforeach()

# Copy the index.theme file
file(INSTALL ${ICON_THEME_DIR}/index.theme
    DESTINATION ${CMAKE_SOURCE_DIR}/resources/icons/${THEME_NAME})
set(FILE_TAGS "${FILE_TAGS}\n        <file>icons/${THEME_NAME}/index.theme</file>")

# Overwrite the Qt resource file
set(RCC_FILE_PATH ${CMAKE_SOURCE_DIR}/resources/${RCC_FILE_NAME})
message(STATUS "Overwriting resource file ${RCC_FILE_PATH}...")
file(WRITE ${RCC_FILE_PATH}
"<!DOCTYPE RCC><RCC version=\"1.0\">
    <qresource>${FILE_TAGS}
    </qresource>
</RCC>
")
