# Breeze Icons theme

The files in this folder are copied from the Breeze icons theme.

## License

Breeze icons copyright KDE and licenced under the GNU LGPL version 3 or later.

https://www.kde.org/products/frameworks/breeze-icons

