/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FATTURAELETTRONICAANTEPRIMA_H
#define FATTURAELETTRONICAANTEPRIMA_H

#include <QMainWindow>

class InvoiceProcessor;
class AttachmentWidgetContainer;
class FatturaElettronica;

class QAction;
class QComboBox;
class QWebEngineView;
class QPrinter;
class QLabel;
class QSettings;
class QDockWidget;

class FatturaElettronicaPreview : public QMainWindow
{
    Q_OBJECT

public:
    explicit FatturaElettronicaPreview (QWidget *parent = nullptr);
    ~FatturaElettronicaPreview() override;
    void loadXml(const QString &path = QString());

public slots:
    void onActionOpenFileTriggered();
    void onActionOpenSettingsTriggered();
    void onActionReloadTriggered();
    void onActionSaveRenameTriggered();
    void onActionExportPdfTriggered();
    void onActionPrintTriggered();
    void onActionAboutFepTriggered();
    void onActionAboutToolsTriggered();
    void onComboboxStyleActivated(int index);
    void onErrorReadingXml(const QString &description);
    void saveAttachment(int invoiceIndex, int attachmentIndex);
    void openAttachment(int invoiceIndex, int attachmentIndex);
    void printerCallback(bool isSuccess);

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    InvoiceProcessor *invoiceProcessor;
    QComboBox *comboboxStyle;
    QString srcFilename;
    QString stylesheetPath;
    // Path of the folder of the last file opened
    QString lastFolderPath;
    QAction *actionSaveRename;
    QAction *actionReload;
    QAction *actionExportPdf;
    QAction *actionPrint;
    QLabel *labelSrcFile;
    QLabel *labelWarning;
    QWebEngineView *view;
    QPrinter *printer;
    QDockWidget *attachmentDock;
    AttachmentWidgetContainer *attachmentContainer;
    QSettings* settings;
    void initGui();
    void loadXslSettings();
    void setMyWindowTitle(const QString &fileName = QString());
    void renderFile(const QString &filename);
    void updateAttachments(const FatturaElettronica *fe);
    void saveSettingsLastFolderPath(const QString &filePath);
};

#endif // FATTURAELETTRONICAANTEPRIMA_H
