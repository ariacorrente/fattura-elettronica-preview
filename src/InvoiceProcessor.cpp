/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QUrl>
#include <QTemporaryFile>
#include <QTemporaryDir>
#include <QXmlStreamReader>
#include <QSettings>
#include <QStandardPaths>
#include <QDesktopServices>

#include "InvoiceProcessor.h"
#include "OpensslTool.h"

#include "fattura-elettronica/FatturaElettronica.h"
#include "settings/DefaultSettings.h"

// Allows the use of "_L1" in QStringView comparison
using namespace Qt::Literals::StringLiterals;

const QString InvoiceProcessor::errorMessageCorruptedXml = tr("corrupted XML file.");

InvoiceProcessor::InvoiceProcessor(QObject* parent)
: QObject(parent)
, useExternalOpenssl(false)
, decodeBase64AlreadyDone(false)
, opensslProcess(nullptr)
, fe(nullptr)
, sslTool(nullptr)
{
    /*
     * Specified filenames can contain the following template XXXXXX (six upper
     * case "X" characters), which will be replaced by the auto-generated
     * portion of the filename.
     * Use XML extension to allow QWebEngineView to detect the XML format.
     *
     * Use an absolute path as template because if the template is a relative
     * path the file will be created in the current working directory. If the
     * original XML file is opened from a network share, security restrictions
     * in the QWebEngineView will stop the XSL to be parsed.
     */
    QString fileTemplate = QDir::temp().absoluteFilePath("XXXXXX.xml");
    tempFile = new QTemporaryFile(fileTemplate, this);

    QString dirTemplate = QDir::temp().absoluteFilePath("fe-preview-XXXXXX");
    tempDir = new QTemporaryDir(dirTemplate);

    // Init random number generator for random file name for extraction from P7M
    srand(time(nullptr));
    extractedFilename = getRandomFilename(QDir::tempPath(), 5, "xml");
    decodedFilename = getRandomFilename(QDir::tempPath(), 5, "p7m");
}

InvoiceProcessor::~InvoiceProcessor()
{
    // Delete the temp file created to extract the XML from the P7M.
    // This should not be normally required, it is here in case of errors.
    deleteFile(extractedFilename);
    deleteFile(decodedFilename);
    delete tempDir;
    delete sslTool;
}

/**
 * @brief Calculates a printable XML invoice from the filename passed.
 *
 * The original invoice is left unchanged, the styled invoce is saved in a new
 * temporarly file.
 *
 * The path of the styled XML will be emitted by the signal styledXmlReady().
 * The path is not simply returned by the method because when this class works
 * on a P7M file the extraction of the XML data may be done with an async
 * execution of a QProcess of the external OpenSSL executable.
 *
 * @param[in] filename The path of the file to process.
 */
void InvoiceProcessor::getPrintable(const QString& filename)
{
    decodeBase64AlreadyDone = false;
    srcFilePath = filename;
    if (filename.endsWith("p7m", Qt::CaseInsensitive)) {
        // First extract the XML from the P7M signed envelop
        extractP7m(filename);
    } else {
        processXml(filename);
    }
}

/*
 * Parse the stream to read content and add a stylesheet.
 *
 * The QXmlStreamReader is created from a file or a buffer by the other
 * processXml methods that will then call this method.
 *
 * The path of the edited XML file will be emitted with the signal
 * styledXmlReady.
 */
void InvoiceProcessor::processXml(QXmlStreamReader& src)
{
    // Output on temporarly file
    if (!tempFile->open()) {
        qWarning() << "Failed to open temp file:" << tempFile->fileName();
        emit errorReadingXml(tr("failed to open the temporarly file required to render the XML."));
        return;
    } else {
        qDebug() << "Using temp filename" << tempFile->fileName();
    }
    // Truncate any existing content
    tempFile->resize(0);

    QXmlStreamWriter dst(tempFile);
    dst.setAutoFormatting(true);

    QXmlStreamReader::TokenType type;
    while (!src.atEnd()) {

        type = src.readNext();
        if (src.hasError()) {
            qWarning() << "Error parsing XML:" << src.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }

        // Cleanup last invoice
        delete fe;
        fe = nullptr;

        if (type == QXmlStreamReader::StartElement) {
            if (src.name() == "FatturaElettronica"_L1) {
                handleFatturaElettronicaFormat(src, dst);
            } else if (src.name() == "RicevutaConsegna"_L1) {
                handleRicevutaConsegnaFormat(src, dst);
            } else if (src.name() == "NotificaEsito"_L1) {
                handleNotificaEsitoFormat(src, dst);
            } else if (src.name() == "FileMetadati"_L1) {
                handleFileMetadatiFormat(src, dst);
            } else {
                handleUnsupportedFormat(src, dst);
            }
        } else if (type == QXmlStreamReader::ProcessingInstruction) {
            qDebug() << "Skipping processing instruction:" << src.name();
            continue;
        }
        dst.writeCurrentToken(src);
    }
    tempFile->flush();
    tempFile->close();

    emit styledXmlReady(tempFile->fileName());
}

/**
 * @brief Process the XML stored in a memory buffer.
 *
 * TODO: No buffer size? Lets hope QXmlStreamReader knows how not to go out of bounds.
 */
void InvoiceProcessor::processXml(const char* inputBuffer)
{
    QXmlStreamReader src(inputBuffer);
    processXml(src);
}

/**
 * @brief Process the XML stored in a file.
 */
void InvoiceProcessor::processXml(const QString& filename)
{
    QFile srcFile(filename);
    if (!srcFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Failed to load XML file:" << filename;
        emit errorReadingXml(tr("failed to open the file."));
        return;
    }
    QXmlStreamReader src(&srcFile);
    processXml(src);
}

void InvoiceProcessor::handleFatturaElettronicaFormat(QXmlStreamReader& reader,
                                                      QXmlStreamWriter& writer)
{
    QXmlStreamReader::TokenType type;

    insertStylesheet(xslPath, writer);

    delete fe;
    fe = new FatturaElettronica;
    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "FatturaElettronicaHeader"_L1) {
                handleFatturaElettronicaHeader(reader, writer);
            } else if (reader.name() == "FatturaElettronicaBody"_L1) {
                handleFatturaElettronicaBody(reader, writer);
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleFatturaElettronicaHeader(QXmlStreamReader &reader,
                                                      QXmlStreamWriter &writer)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "CedentePrestatore"_L1) {
                handleCedentePrestatore(reader, writer);
            } else if (reader.name() == "CessionarioCommittente"_L1) {
                handleCessionarioCommittente(reader, writer);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "FatturaElettronicaHeader"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleCedentePrestatore(QXmlStreamReader &reader,
                                               QXmlStreamWriter &writer)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "DatiAnagrafici"_L1) {
                handleDatiAnagrafici(reader, writer, fe->header.cedentePrestatore.datiAnagrafici);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "CedentePrestatore"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleCessionarioCommittente(QXmlStreamReader &reader,
                                                    QXmlStreamWriter& writer)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "DatiAnagrafici"_L1) {
                handleDatiAnagrafici(reader, writer, fe->header.cessionarioCommittente.datiAnagrafici);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "CessionarioCommittente"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleDatiAnagrafici(QXmlStreamReader &reader,
                                            QXmlStreamWriter &writer,
                                            DatiAnagrafici &data)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "Anagrafica"_L1) {
                handleAnagrafica(reader, writer, data.anagrafica);
            } else if (reader.name() == "IdFiscale"_L1) {
                handleIdFiscaleIva(reader, writer, data.idFiscaleIVA);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "DatiAnagrafici"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleAnagrafica(QXmlStreamReader &reader,
                                        QXmlStreamWriter &writer,
                                        Anagrafica &anagrafica)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "Denominazione"_L1) {
                anagrafica.denominazione = handleCharacters(reader, writer);
            } else if (reader.name() == "Nome"_L1) {
                anagrafica.nome = handleCharacters(reader, writer);
            } else if (reader.name() == "Cognome"_L1) {
                anagrafica.cognome = handleCharacters(reader, writer);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "Anagrafica"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleIdFiscaleIva(QXmlStreamReader &reader,
                                       QXmlStreamWriter &writer,
                                       IdFiscaleIVA& id)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "IdPaese"_L1) {
                id.idPaese= handleCharacters(reader, writer);
            } else if (reader.name() == "IdCodice"_L1) {
                id.idCodice = handleCharacters(reader, writer);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "IdFiscale"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}
void InvoiceProcessor::handleFatturaElettronicaBody(QXmlStreamReader& reader,
                                                    QXmlStreamWriter& writer)
{
    QXmlStreamReader::TokenType type;

    FatturaElettronicaBody *body = new FatturaElettronicaBody;
    fe->bodies.append(body);

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "DatiGenerali"_L1) {
                handleDatiGenerali(reader, writer, *body);
            } else if (reader.name() == "Allegati"_L1) {
                handleAllagati(reader, writer, *body);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "FatturaElettronicaBody"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleDatiGenerali(QXmlStreamReader& reader,
                                          QXmlStreamWriter& writer,
                                          FatturaElettronicaBody &body)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "DatiGeneraliDocumento"_L1) {
                handleDatiGeneraliDocumento(reader, writer, body.datiGenerali);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "DatiGenerali"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleDatiGeneraliDocumento(QXmlStreamReader& reader,
                                                   QXmlStreamWriter& writer,
                                                   DatiGenerali &datiGenerali)
{
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "Numero"_L1) {
                datiGenerali.datiGeneraliDocumento.numero = handleCharacters(reader, writer);
            }
            if (reader.name() == "Data"_L1) {
                datiGenerali.datiGeneraliDocumento.data = handleCharacters(reader, writer);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "DatiGeneraliDocumento"_L1) {
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleAllagati(QXmlStreamReader& reader,
                                      QXmlStreamWriter& writer,
                                      FatturaElettronicaBody &body)
{
    Allegati *allegati = new Allegati();
    QXmlStreamReader::TokenType type;

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        type = reader.readNext();
        if (type == QXmlStreamReader::StartElement) {
            if (reader.name() == "NomeAttachment"_L1) {
                allegati->name = handleCharacters(reader, writer);
            } else if (reader.name() == "Attachment"_L1) {
                QByteArray encoded(handleCharacters(reader, writer));
                allegati->content = QByteArray::fromBase64(encoded);
            }
        } else if (type == QXmlStreamReader::EndElement) {
            if (reader.name() == "Allegati"_L1) {
                body.allegati.append(allegati);
                return;
            }
        }
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

QByteArray InvoiceProcessor::handleCharacters(QXmlStreamReader& reader, QXmlStreamWriter& writer)
{
    QXmlStreamReader::TokenType type;

    writer.writeCurrentToken(reader);
    type = reader.readNext();
    if (type == QXmlStreamReader::Characters) {
        return reader.text().toUtf8();
    }
    if (reader.hasError()) {
        qWarning() << "Error parsing XML:" << reader.errorString();
        emit errorReadingXml(errorMessageCorruptedXml);
    }
    return QByteArray();
}

void InvoiceProcessor::handleFileMetadatiFormat(QXmlStreamReader& reader, QXmlStreamWriter& writer)
{
    handleMessaggiFormat(reader, writer, "FileMetadati.xsl");
}

void InvoiceProcessor::handleNotificaEsitoFormat(QXmlStreamReader& reader, QXmlStreamWriter& writer)
{
    handleMessaggiFormat(reader, writer, "NotificaEsito.xsl");
}

void InvoiceProcessor::handleRicevutaConsegnaFormat(QXmlStreamReader& reader, QXmlStreamWriter& writer)
{
    handleMessaggiFormat(reader, writer, "RicevutaConsegna.xsl");
}

void InvoiceProcessor::handleMessaggiFormat(QXmlStreamReader& reader, QXmlStreamWriter& writer, const QString& xslFilename)
{

    QString xslPath = QStandardPaths::locate(QStandardPaths::AppDataLocation, xslFilename);
    if (xslPath.isEmpty()) {
        qWarning() << "XSL stylesheet not found:" << xslFilename;
        emit errorReadingXml(tr("XSL stylesheet not found."));
    } else {
        insertStylesheet(xslPath, writer);
    }

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        reader.readNext();
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

void InvoiceProcessor::handleUnsupportedFormat(QXmlStreamReader& reader, QXmlStreamWriter& writer)
{
    qWarning() << "XML format not supported";

    while (!reader.atEnd()) {
        writer.writeCurrentToken(reader);
        reader.readNext();
        if (reader.hasError()) {
            qWarning() << "Error parsing XML:" << reader.errorString();
            emit errorReadingXml(errorMessageCorruptedXml);
            return;
        }
    }
}

/**
 * TODO: what to do if the XML already have a stylesheet?
 */
void InvoiceProcessor::insertStylesheet(const QString xslPath, QXmlStreamWriter& writer)
{
    if (xslPath.isEmpty()) {
        // Nothing to do
    } else if (QFile(xslPath).exists()) {
        writer.writeProcessingInstruction("xml-stylesheet type=\"text/xsl\" href=\"" + xslPath + "\"");
    } else {
        qWarning() << "XSL stylesheet not found:" << xslPath;
        emit errorReadingXml(tr("XSL stylesheet not found."));
    }
}

/*
 * Extract the file inside a P7M envelop using an external openSSL executable.
 * The process is async, the result will be handled by the slot
 * onOpensslFinished.
 */
void InvoiceProcessor::extractP7m(const QString& filename)
{
    if (useExternalOpenssl) {
        extractP7mOpenssl(filename);
    } else {
        extractP7mInMemory(filename);
    }
}

/*
 * Extract p7m with an external OpenSSL executable.
 */
void InvoiceProcessor::extractP7mOpenssl(const QString& filename)
{
    QSettings settings;
    QString program = settings.value("openssl-path", DefaultSettings::opensslPath).toString();
    QStringList arguments;
    arguments << "smime" << "-verify"
        /*
         * Skip signers certificate or openssl will always fail because it
         * cannot find the trusted certificates
         */
        << "-noverify"
        /*
         * Skip message signature check or openssl will fail sometimes.
         * Maybe a bug in openssl version 1.1.0x, some references in:
         * https://github.com/OCA/l10n-italy/pull/788
         */
        << "-nosigs"
        << "-inform" << "der "
        << "-in" << filename
        << "-out" << extractedFilename;

    qDebug() << "Executing " << program << "with arguments:" << arguments;

    if (opensslProcess == nullptr) {
        opensslProcess = new QProcess(this);
        connect(opensslProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
                this, &InvoiceProcessor::onOpensslFinished);
        connect(opensslProcess, QOverload<QProcess::ProcessError>::of(&QProcess::errorOccurred),
                this, &InvoiceProcessor::onOpensslError);
    }

    opensslProcess->start(program, arguments);
}

/*
 * Extract p7m with the included OpenSSL library.
 *
 * This will open the file only one time and should be faster than using an
 * external OpenSSL executable that needs to work on multiple files.
 */
void InvoiceProcessor::extractP7mInMemory(const QString& filename)
{
    qint64 fileSize;
    qint64 bytesRead;
    long bufferSize;
    char* inputBuffer;
    char* outputBuffer = nullptr;
    QByteArray encodedData;
    QByteArray::FromBase64Result decodeResult;

    if (sslTool == nullptr) {
        sslTool = new OpensslTool;
    }

    QFile p7mFile(filename);
    if (!p7mFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Error opening file" << filename;
        emit errorReadingXml(tr("failed to open the file."));
        return;
    }
    fileSize = p7mFile.size();

    // What if the invoice is too big for the available memory?
    try {
        inputBuffer = new char[fileSize];
    } catch(std::bad_alloc& e) {
        qWarning() << "Failed to allocate" << fileSize << " bytes of memory for file" << filename;
        p7mFile.close();
        return;
    }
    bytesRead = p7mFile.read(inputBuffer, fileSize);
    if (bytesRead != fileSize) {
        qWarning() << "Read only" << bytesRead << "bytes from a file of size" << fileSize << "bytes";
        // TODO: file is truncated... continue or not?
    }

    bufferSize = sslTool->extractP7m(inputBuffer, fileSize, &outputBuffer);
    if (bufferSize != -1) {
        processXml(outputBuffer);
    } else {
        // Failed to extract, maybe it is encoded in base64, try to decode first
        encodedData = QByteArray::fromRawData(inputBuffer, fileSize);
        decodeResult = QByteArray::fromBase64Encoding(encodedData);
        if (decodeResult.decodingStatus == QByteArray::Base64DecodingStatus::Ok) {
            qDebug() << "base4 decode success";
        } else {
            qWarning() << "base4 decode error";
            delete inputBuffer;
            p7mFile.close();
            return;
        }
        // Try to extract again from the p7m
        const QByteArray& decoded = decodeResult.decoded;
        bufferSize = sslTool->extractP7m(decoded.constData(), decoded.size(), &outputBuffer);
        if (bufferSize != -1) {
            processXml(outputBuffer);
        } else {
            qWarning() << "Failed to decode p7m even after base64 decode";
            emit errorReadingXml(tr("failed to decode P7M format."));
        }
    }
    delete inputBuffer;
    p7mFile.close();
}

/*
 * Slot called when the executable openSSL finishes.
 */
void InvoiceProcessor::onOpensslFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    // openssl error status is passed in exitCode, exitStatus is about QProcess.
    if (exitCode == 0) {
        processXml(extractedFilename);
    } else {
        qWarning() << "OpenSSL executable exited exit code:" << exitCode;
        // Maybe its base64 encoded?
        if (decodeBase64AlreadyDone) {
            deleteFile(extractedFilename);
            deleteFile(decodedFilename);
            emit errorReadingXml(tr("the openssl executable exited with and error."));
        } else {
            qDebug() << "Maybe it is base64 encoded, trying to decode" << srcFilePath << "as" << decodedFilename;
            decodeBase64(srcFilePath, decodedFilename);
            extractP7m(decodedFilename);
            deleteFile(extractedFilename);
        }
    }
}

void InvoiceProcessor::onOpensslError(QProcess::ProcessError error)
{
    emit errorReadingXml(tr("failed to launch the openssl executable."));
}

/*
 * Calculates a random filename to be usea as temporarly file.
 *
 * The filename is checked for not to be in use.
 * If the method fails an empty string will be returned. This can happen when
 * all the filenames tested are in use.
 *
 * @param basePath Base folder for the file
 * @param length Number of characters of the filename
 * @param extension Extension to the file to be added at the end of the filename
 * @return The path of the file
 */
QString InvoiceProcessor::getRandomFilename(const QString &basePath, int length, const QString &extension)
{
    QString returnPath;
    // If the proposed filename is already in use, loop again for a new one
    const int maxRetries = 100;
    int retryCount = 0;
    bool isFilenameUsed;
    QByteArray buffer(length, '\0');
    // Pool of characters to use as source of random characters
    const char pool[] = "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    const int poolSize = sizeof(pool) - 1;

    if (length <= 0) {
        qWarning() << "Length must be > 0:" << length;
        return QString();
    }

    do {
        // Get a number of 'length' random alphanumeric characters
        for (int i = 0; i < length; ++i) {
        buffer[i] = pool[rand() % poolSize];
        }

        // Build an absolute path for temporarly file
        const QString fileName = QString("%1.%2").arg(QLatin1String(buffer)).arg(extension);
        QDir dir(basePath);
        returnPath = dir.filePath(fileName);

        // Check if the filename is new or if it is already in use
        isFilenameUsed = QFile::exists(returnPath);
        if (isFilenameUsed) {
            returnPath.clear();
            qDebug() << "Discarted random filename because already in use:" << returnPath;
        }
        retryCount++;
    } while ( !isFilenameUsed && retryCount < maxRetries);

    return returnPath;
}

void InvoiceProcessor::deleteFile(const QString& filename)
{
    QFile file(filename);
    if(file.exists()) {
        bool isRemoved = file.remove();
        if (!isRemoved) {
            qWarning() << "Error removing temp file:" << filename;
        }
    }
}

/**
 * Decode from Base64 from a file and writes the decoded data into another file
 *
 * @param srcPath Path of the file to decode
 * @param dstPath Path of the file to write
  */
void InvoiceProcessor::decodeBase64(const QString &srcPath, const QString &dstPath)
{
    decodeBase64AlreadyDone = true;
    QFile srcFile(srcPath);
    if (!srcFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Failed to open file to read and decode from Base64:" << srcPath;
        return;
    }

    deleteFile(dstPath);
    QFile dstFile(dstPath);
    if (!dstFile.open(QIODevice::WriteOnly)) {
        qWarning() << "Failed to open file to write decoded from Base64:" << srcPath;
        srcFile.close();
        return;
    }

    // TODO: handle huge files
    QByteArray srcBuffer= srcFile.readAll();
    QByteArray decoded = QByteArray::fromBase64(srcBuffer);
    dstFile.write(decoded);

    srcFile.close();
    dstFile.close();
}

void InvoiceProcessor::saveAttachment(int invoiceIndex, int attachmentIndex, const QString& path)
{
    if (fe->bodies.count() > invoiceIndex &&
        fe->bodies[invoiceIndex]->allegati.count() > attachmentIndex) {
        QFile file(path);
        if (!file.open(QIODevice::WriteOnly)) {
            qWarning() << "Failed to open file";
            return;
        }
        file.write(fe->bodies[invoiceIndex]->allegati[attachmentIndex]->content);
        file.close();
    }
}

/**
 * TODO: is this way safe?
 */
void InvoiceProcessor::openAttachment(int invoiceIndex, int attachmentIndex)
{
    if (fe->bodies.count() > invoiceIndex &&
        fe->bodies[invoiceIndex]->allegati.count() > attachmentIndex &&
        tempDir->isValid()) {

        QString filename = tempDir->filePath(fe->bodies[invoiceIndex]->allegati[attachmentIndex]->name);
        saveAttachment(invoiceIndex, attachmentIndex, filename);
        QUrl url = QUrl::fromLocalFile(filename);
        QDesktopServices::openUrl(url);
    }
}
