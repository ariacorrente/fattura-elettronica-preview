/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpensslTool.h"

#include <QDebug>
#include <QString>

#include <openssl/ssl.h>
#include <openssl/err.h>

// This should fix an error about applink under windows
#ifdef _WIN32
    #include <openssl/applink.c>
#endif

OpensslTool::OpensslTool()
: outputBio(NULL)
{
    SSL_load_error_strings();
    SSL_library_init();
}

OpensslTool::~OpensslTool()
{
    BIO_free(outputBio);
}

/**
 * @brief Extracts the content of a p7m file already loaded in memory.
 *
 * @param inputBuffer[in] Pointer to the data to be extracted.
 * @param size[in] Size of the inputBuffer.
 * @param outputBuffer[out] Pointer to the buffer of the extracted data.
 * @return Size of the outputBuffer or -1 on error.
 */
long OpensslTool::extractP7m(const char* inputBuffer,
                             qint64 size,
                             char** outputBuffer)
{
    BIO* inputBio = NULL;
    PKCS7 *p7 = NULL;
    long outputSize = 0;

    /*
     * Clear the previously allocated bio.
     * TODO: Is this required or can it be recycled?
     */
    BIO_free(outputBio);

    /*
     * Create the input BIO
     * BIO_new_file() and BIO_new_fp() return a file BIO or NULL if an error
     * occurred.
     */
    inputBio = BIO_new_mem_buf(inputBuffer, size);
    if (inputBio == NULL) {
        qWarning() << "BIO_new_mem_buf FAILED on input BIO";
        ERR_print_errors_fp(stderr);
        BIO_free(inputBio);
        BIO_free(outputBio);
        outputBio = NULL;
        return errorCode;
    } else {
        qDebug() << "BIO_new_mem_buf success on input BIO";
    }

    // Create the output BIO
    outputBio = BIO_new(BIO_s_mem());
    if (outputBio == NULL) {
        qWarning() << "BIO_new FAILED on output buffer";
        ERR_print_errors_fp(stderr);
        BIO_free(inputBio);
        BIO_free(outputBio);
        outputBio = NULL;
        return errorCode;
    } else {
        qDebug()  << "BIO_new success on output buffer";
    }

    /*
     * d2i_TYPE(), d2i_TYPE_bio() and d2i_TYPE_fp() return a valid TYPE
     * structure or NULL if an error occurs
     */
    p7 = d2i_PKCS7_bio(inputBio, NULL);
    if (p7 == NULL) {
        qWarning() << "d2i_PKCS7_bio FAILED to read p7m file";
        ERR_print_errors_fp(stderr);
        BIO_free(inputBio);
        BIO_free(outputBio);
        outputBio = NULL;
        return errorCode;
    } else {
        qDebug() << "d2i_PKCS7_bio success reading p7m file";
    }

    /*
     * TODO: is this the correct way to get the content of the p7m?
     *
     * ASN1_STRING_print_ex() and ASN1_STRING_print_ex_fp() return the number
     * of characters written or -1 if an error occurred.
     */
    outputSize = ASN1_STRING_print_ex(outputBio,
                                      p7->d.sign->contents->d.data,
                                      ASN1_STRFLGS_DUMP_DER);
    if (outputSize == -1) {
        qWarning() << "ASN1_STRING_print_ex failed";
        ERR_print_errors_fp(stderr);
        BIO_free(inputBio);
        BIO_free(outputBio);
        outputBio = NULL;
        return errorCode;
    } else {
        qDebug() << "ASN1_STRING_print_ex success, number of characters written:" << outputSize;
    }

    /*
     * Extract the buffer from the output BIO
     * BIO_get_mem_data() sets *pp to a pointer to the start of the memory BIOs
     * data and returns the total amount of data available.
     */
    outputSize = BIO_get_mem_data(outputBio, outputBuffer);
    if (outputSize == -1) {
        qWarning() << "BIO_get_mem_data failed";
        ERR_print_errors_fp(stderr);
        BIO_free(inputBio);
        BIO_free(outputBio);
        outputBio = NULL;
        return errorCode;
    } else {
        qDebug() << "BIO_get_mem_data success with size:" << outputSize;
    }

    BIO_free(inputBio);
    // Do not free inputBio now or the returned buffer will be cleared too
    return outputSize;
}
