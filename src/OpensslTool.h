/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENSSLTOOL_H
#define OPENSSLTOOL_H

// Required for qint64
#include <QtCore>

#include  "openssl/bio.h"

/**
 * @todo Handle signed invoices in p7m format.
 */
class OpensslTool
{
public:
    OpensslTool();
    ~OpensslTool();

    long extractP7m(const char* inputBuffer, qint64 size,
                   char** outputBuffer);

private:
    static const int errorCode = -1;
    BIO* outputBio;
};

#endif // OPENSSLTOOL_H
