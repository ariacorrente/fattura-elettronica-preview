/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PatternReplacer.h"

#include "fattura-elettronica/FatturaElettronica.h"

#include <QDir>
#include <QFileInfo>
#include <QRegularExpression>

PatternReplacer::PatternReplacer()
: invoice(nullptr)
, invoiceIndex(-1)
{
}

PatternReplacer::PatternReplacer(const PatternReplacer &other)
{
    copyPatternReplacer(other);
}

PatternReplacer& PatternReplacer::operator=(const PatternReplacer& other)
{
    copyPatternReplacer(other);
    return *this;
}

PatternReplacer::~PatternReplacer()
{

}

void PatternReplacer::copyPatternReplacer(const PatternReplacer& other)
{
    invoiceIndex = other.invoiceIndex;
    invoice = other.invoice;
    fileName = other.fileName;
    output = other.output;
}

void PatternReplacer::setInvoiceIndex(int index)
{
    invoiceIndex = index;
}

void PatternReplacer::setInvoice(const FatturaElettronica *invoice)
{
    this->invoice = invoice;
}

void PatternReplacer::setFileName(const QString &fileName)
{
    this->fileName = fileName;
}

void PatternReplacer::setData(const FatturaElettronica *invoice,
                              int invoiceIndex,
                              const QString &fileName)
{
    setInvoice(invoice);
    setInvoiceIndex(invoiceIndex);
    setFileName(fileName);
}

QString PatternReplacer::normalizeDenominazione(const QString &name)
{
    QString output = name;
    // TODO: make this collection of strigs as a configuration for the user?
    QStringList skipStrings = {
        // Standard italian company types
        "S.S.", "S.N.C.", "S.A.S.", "S.P.A.", "S.A.P.A.", "S.C.P.A.",
        "S.C.A.R.L.", "S.R.L.", "S.C.R.L.",
        // Most used italian company types
        "SOC. COOP. A.R.L.",
        // Most used standard international company types
        "G.M.B.H.", "L.T.D.", "L.L.C.", "S.à.r.l."
    };
    // Transform to regex
    QStringList::Iterator iter  = skipStrings.begin();
    while (iter != skipStrings.end()) {
        *iter = iter->append('$');          // At the end of the string
        *iter = iter->prepend("\\s");       // After a space
        *iter = iter->replace(".", "\\.?"); // The dot is optional
        *iter = iter->replace(" ", "\\s");  // Replace space with \s
        ++iter;
    }

    QStringList::ConstIterator consIter  = skipStrings.constBegin();
    QRegularExpression rx;
    while (consIter != skipStrings.constEnd()) {
        rx = QRegularExpression(*consIter, QRegularExpression::CaseInsensitiveOption);
        output = output.replace(rx, "");
        ++consIter;
    }

    // Skip user strings
    consIter = userSkipStrings.constBegin();
    while (consIter != userSkipStrings.constEnd()) {
        output = output.replace(*consIter, "");
        ++consIter;
    }

    // Remove duplicate spaces and trim
    output = output.simplified();
    output = output.replace(' ', '-');
    output = output.replace(QDir::separator(), '-');
    return output;
}

void PatternReplacer::initTokens()
{
    tokens.clear();

    QFileInfo fileInfo(QDir::toNativeSeparators(fileName));
    QString separator = QDir::separator();

    if (invoice != nullptr && invoiceIndex < invoice->bodies.count()) {
        QString date = invoice->bodies[invoiceIndex]->datiGenerali.datiGeneraliDocumento.data;
        QStringList dateParts = date.split('-');
        tokens["d"] = invoice->bodies[invoiceIndex]->datiGenerali.datiGeneraliDocumento.data;
        if (dateParts.count() == 3) {
            tokens["Y"] = dateParts.at(0);
            tokens["M"] = dateParts.at(1);
            tokens["D"] = dateParts.at(2);
        }
        tokens["n"] = invoice->bodies[invoiceIndex]->datiGenerali.datiGeneraliDocumento.numero;
    }

    tokens["a"] = invoice->header.cedentePrestatore.datiAnagrafici.anagrafica.denominazione;
    tokens["A"] = normalizeDenominazione(tokens["a"]);
    tokens["b"] = invoice->header.cessionarioCommittente.datiAnagrafici.anagrafica.denominazione;
    tokens["B"] = normalizeDenominazione(tokens["b"]);

    // Only the name of the file, without extension or folder path
    tokens["f"] = fileInfo.baseName();
    // Absolute path of the folder where the invoice is saved
    tokens["F"] = fileInfo.absolutePath();
    // File extension, usually "xml" or "xml.p7m"
    tokens["x"] = fileInfo.completeSuffix();
    // Directory separator, usually "/" or "\" depending on the operating system
    tokens["s"] = separator;
    // Escape "%" character
    tokens["%"] = "%";
}

void PatternReplacer::runPattern(const QString &pattern)
{
    int size = pattern.size();

    output.clear();
    initTokens();

    for (int i = 0; i < size; ++i) {
        if (pattern.at(i) == '%') {
            ++i;
            // Check if '%' was the last character in the string
            if (i < size) {
                // Replace the token
                output.append(tokens[pattern.at(i)]);
            } else {
                // Skip the leading %
            }
        } else {
            // This is a normal character, simply copy to the output string
            output.append(pattern.at(i));
        }
    }
}
