/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PATTERNREPLACER_H
#define PATTERNREPLACER_H

#include <QMap>
#include <QString>

class FatturaElettronica;

/**
 * Reads a string to replace predefined tokens with data from the invoice.
 *
 * Everything that is not a token is copied to the output string.
 * If a token tries to get data not available or corrupted will be translated in
 * an empty string, no error will be thrown.
 */
class PatternReplacer
{
public:
    PatternReplacer();
    PatternReplacer(const PatternReplacer &other);
    ~PatternReplacer();
    PatternReplacer& operator=(const PatternReplacer &other);

    QString getOutputString() const { return output; };
    void setInvoice(const FatturaElettronica *invoice);
    void setInvoiceIndex(int index);
    void setFileName(const QString &fileName);
    void setUserSkipStrings(const QStringList strings) {userSkipStrings = strings;};
    void setData(const FatturaElettronica *invoice,
                 int invoiceIndex,
                 const QString &fileName);
    void runPattern(const QString &pattern);

private:
    const FatturaElettronica* invoice;
    int invoiceIndex;
    QMap <QString, QString> tokens;
    QString fileName;
    QString output;
    QStringList userSkipStrings;

    void initTokens();
    void copyPatternReplacer(const PatternReplacer &other);
    QString normalizeDenominazione(const QString &name);
};

#endif // PATTERNREPLACER_H
