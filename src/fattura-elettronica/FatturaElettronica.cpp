/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FatturaElettronica.h"

/*
 * FatturaElettronicaBody
 */
FatturaElettronicaBody::FatturaElettronicaBody()
{
}

FatturaElettronicaBody::FatturaElettronicaBody(const FatturaElettronicaBody &other)
{
    copyBody(other);
}

FatturaElettronicaBody& FatturaElettronicaBody::operator=(const FatturaElettronicaBody &other)
{
    copyBody(other);
    return *this;
}

FatturaElettronicaBody::~FatturaElettronicaBody()
{
    for (int i = 0; i < allegati.count(); ++i) {
        delete allegati[i];
    }
    allegati.clear();
}

void FatturaElettronicaBody::copyBody(const FatturaElettronicaBody &other)
{
    datiGenerali = other.datiGenerali;
    for (int i = 0; i < other.allegati.count(); ++i) {
        allegati.append(new Allegati(*other.allegati[i]));
    }
}

/*
 * FatturaElettronica
 */
FatturaElettronica::FatturaElettronica()
{
}

FatturaElettronica::FatturaElettronica(const FatturaElettronica &other)
{
    copyInvoice(other);
}

FatturaElettronica& FatturaElettronica::operator=(const FatturaElettronica &other)
{
    copyInvoice(other);
    return *this;
}

FatturaElettronica::~FatturaElettronica()
{
    for (int i = 0; i < bodies.count(); ++i) {
        delete bodies[i];
    }
    bodies.clear();
}

void FatturaElettronica::copyInvoice(const FatturaElettronica &other)
{
    header = other.header;
    for (int i = 0; i < other.bodies.count(); ++i) {
        bodies.append(new FatturaElettronicaBody(*(other.bodies[i])));
    }
}
