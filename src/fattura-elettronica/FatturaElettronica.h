/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FATTURAELETTRONICA_H
#define FATTURAELETTRONICA_H

#include <QMetaType>
#include <QVector>

class IdFiscaleIVA
{
public:
    QString idPaese;
    QString idCodice;
};

class Anagrafica
{
public:
    QString denominazione;
    QString nome;
    QString cognome;
    QString titolo;
    QString codEORI;
};

class DatiAnagrafici
{
public:
    IdFiscaleIVA idFiscaleIVA;
    QString CodiceFiscale;
    Anagrafica anagrafica;
    QString alboProfessionale;
    QString provinciaAlbo;
    QString numeroIscrizioneAlbo;
    QString dataIscrizioneAlbo;
    QString regimeFiscale;
};

class CedentePrestatore
{
public:
    DatiAnagrafici datiAnagrafici;
};

class CessionarioCommittente
{
public:
    DatiAnagrafici datiAnagrafici;
};

class FatturaElettronicaHeader
{
public:
    CedentePrestatore cedentePrestatore;
    CessionarioCommittente cessionarioCommittente;
};

class DatiGeneraliDocumento
{
public:
    QString numero;
    QString data;
};

class DatiGenerali
{
public:
    DatiGeneraliDocumento datiGeneraliDocumento;
};

class Allegati
{
public:
    QString description;
    QString name;
    QByteArray content;
};

class FatturaElettronicaBody
{
public:
    DatiGenerali datiGenerali;
    QVector<Allegati*> allegati;

    FatturaElettronicaBody();
    FatturaElettronicaBody(const FatturaElettronicaBody &other);
    FatturaElettronicaBody& operator=(const FatturaElettronicaBody &other);
    ~FatturaElettronicaBody();

private:
    void copyBody(const FatturaElettronicaBody &other);
};

class FatturaElettronica
{

public:
    FatturaElettronicaHeader header;
    QVector<FatturaElettronicaBody*> bodies;

    FatturaElettronica();
    FatturaElettronica(const FatturaElettronica &other);
    FatturaElettronica& operator=(const FatturaElettronica &other);
    ~FatturaElettronica();

private:
    void copyInvoice(const FatturaElettronica &other);
};

// Declare metatype to use the class in QTest
Q_DECLARE_METATYPE(FatturaElettronica)

#endif // FATTURAELETTRONICA_H
