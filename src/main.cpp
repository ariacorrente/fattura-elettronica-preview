/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FatturaElettronicaPreview.h"

#include <QApplication>
#include <QTranslator>
#include <QSettings>
#include <QDebug>
#include <QLibraryInfo>
#include <QStandardPaths>
#include <QDir>

int main(int argc, char *argv[])
{
    // If the webview is empty it may be caused by the Chrome sandbox.
    // Disabling the sandbox should fix the problem, more info at:
    // https://doc.qt.io/Qt-5/qtwebengine-platform-notes.html#sandboxing-support
    // qputenv("QTWEBENGINE_CHROMIUM_FLAGS", "--no-sandbox");

    QApplication app(argc, argv);
    // Setting the applicaton icon here allows the about dialog to detect the
    // correct icon to display aside the description.
    app.setWindowIcon(QIcon(":app-icon"));
    QCoreApplication::setOrganizationDomain("dominiofelice.com");
    QCoreApplication::setApplicationName("fe-preview");

    QSettings settings;
    QString systemLocale = QLocale::system().name();
    QLocale locale = QLocale(settings.value("locale", systemLocale).toString());
    QString qtTranslationsPath;
    bool isOk;

    // Load translation for Qt strings from the system path
    QTranslator qtTranslator;
    qtTranslationsPath = QLibraryInfo::path(QLibraryInfo::TranslationsPath);
    isOk = qtTranslator.load(locale, "qt", "_", qtTranslationsPath);
    app.installTranslator(&qtTranslator);
    if (isOk) {
        qDebug() << "Qt5 translations loaded from" << qtTranslationsPath;
    } else {
        qWarning() << "Failed to load Qt5 translations from" << qtTranslationsPath;
    }

    // Load translation for Qt strings from the installation folder of the
    // application, required for windows installations
    QTranslator qtTranslatorLocal;
    qtTranslationsPath = QCoreApplication::applicationDirPath();
    QDir dirTranslations(QCoreApplication::applicationDirPath());
    dirTranslations.cd("translations");
    isOk = qtTranslatorLocal.load(locale, "qt", "_", dirTranslations.absolutePath());
    if (isOk) {
        app.installTranslator(&qtTranslatorLocal);
        qDebug() << "Qt5 translations loaded from" << qtTranslationsPath;
    }
    else {
        qWarning() << "Failed to load Qt5 translations from" << qtTranslationsPath;
    }

    // Load translation for this program
    QTranslator myTranslator;
    QStringList folders = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    // Search also in the folder where the executable is saved This is required
    // for windows because the installer saves all in the same folder.
    folders.append(QCoreApplication::applicationDirPath());
    QStringList::const_iterator iter = folders.constBegin();
    while (iter != folders.constEnd()) {
        isOk = myTranslator.load(locale, "lang", "_", *iter);
        if (isOk) {
            qDebug() << "fe-preview translations loaded from" << *iter;
            app.installTranslator(&myTranslator);
            break;
        } else {
            qWarning() << "Failed to load" << locale.name()
                <<"translations for fe-preview from" << *iter;
        }
        ++iter;
    }

    QIcon::setFallbackThemeName("Breeze");

    QString firstArg;
    FatturaElettronicaPreview window;

    if (argc > 1) {
        firstArg = QString::fromLocal8Bit(argv[1]);
        window.loadXml(firstArg);
    }
    window.show();

    return app.exec();
}
