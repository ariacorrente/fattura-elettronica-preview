/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DefaultSettings.h"

/*
 * Locale
 */
const QLocale DefaultSettings::locale = QLocale(QLocale::Italian, QLocale::Italy);

const bool DefaultSettings::useExternalOpenssl = false;
/*
 * OpenSSL executable path
 */
#ifdef unix
    const QString DefaultSettings::opensslPath = "/usr/bin/openssl";
#endif

#ifdef _WIN32
    // No default installation path found for windows
    const QString DefaultSettings::opensslPath = "C:\\Program Files\\OpenSSL-Win64\\bin\\openssl.exe";
#endif

const QString DefaultSettings::patternSave = "%F%s%Y%M%D_%f.%x";
const QString DefaultSettings::patternExportPdf = "%F%s%Y%M%D_%A_DESCRIPTION.pdf";
const bool DefaultSettings::renameEnabled = false;

const QString DefaultSettings::userSkipStrings = "& Co.";

DefaultSettings::DefaultSettings()
{

}
