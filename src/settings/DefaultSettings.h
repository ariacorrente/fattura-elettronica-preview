/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DEFAULTSETTINGS_H
#define DEFAULTSETTINGS_H

#include <QString>
#include <QLocale>

class DefaultSettings
{
public:
    DefaultSettings();
    /**
     * What OpenSSL version to use:
     * - true: will be used the external OpenSSL executable found in "opensslPath"
     * - false: will be used the faster internal library
     * The external executable should be slower because it works on files while
     * the internal library works in memory.
     */
    static const bool useExternalOpenssl;
    /// Path of the external OpenSSL executable.
    static const QString opensslPath;
    /// Default pattern for the save/rename dialog.
    static const QString patternSave;
    /// Default pattern for the export PDF dialog.
    static const QString patternExportPdf;
    /// If the checkbox "Rename" in the save/rename dialog is checked
    static const bool renameEnabled;
    /// Custom strings to be removed on the normalization phase of the pattern
    /// replacement
    static const QString userSkipStrings;
    static const QLocale locale;
};

#endif // DEFAULTSETTINGS_H
