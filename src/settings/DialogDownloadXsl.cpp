/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DialogDownloadXsl.h"

#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QDialogButtonBox>
#include <QTemporaryFile>
#include <QInputDialog>
#include <QProgressBar>
#include <QSettings>
#include <QTimer>

#include <JlCompress.h>

DialogDownloadXsl::DialogDownloadXsl(QWidget *parent)
{
    timerTimeout = new QTimer(this);
    timerTimeout->setSingleShot(true);
    // 10 seconds of timeout
    // The files to download are tiny, a too long timeout will probably set the
    // user in click-rage-mode
    timerTimeout->setInterval(10000);
    connect(timerTimeout, &QTimer::timeout,
            this, &DialogDownloadXsl::onRequestTimeout);

    initGui();
}

void DialogDownloadXsl::initGui()
{
    setWindowTitle(tr("Download stylesheets"));

    /*
     * Intro label
     */
    QString intro;
    intro = tr("This tool allows to download some stylesheets available online.\n"
        "These stylesheets are not packaged with this program because they are developed by others.\n"
    );
    QLabel *labelIntro = new QLabel(intro, this);

    /*
     * AssoSoftware label
     */
    QString introAssoSoftware;
    introAssoSoftware = tr("<strong>Stylesheet from AssoSoftware</strong><br>"
    "Before downloading and using this stylesheet you need to accept the license and the terms of use<br>"
    "available at this address: <a href=\"http://www.assosoftware.it/assoinvoice\">http://www.assosoftware.it/assoinvoice</a>.");
    QLabel *labelAssoSoftware = new QLabel(introAssoSoftware, this);

    buttonAssoSoftware = new QPushButton(tr("Download from AssoSoftware"), this);
    connect(buttonAssoSoftware, &QPushButton::clicked,
            this, &DialogDownloadXsl::onButtonAssoSoftwareClicked);

    /*
     * Agenzia Entrate label
     */
    QString introEntrate = tr("<strong>Stylesheets from \"Agenzia delle Entrate\"</strong><br>"
    "The italian \"Agenzia delle Entrate\" published two stylesheets:<br>"
    "- stylesheet for for invoices in the format \"pubblica amministrazione\";<br>"
    "- stylesheet for invoices in format \"fatture ordinarie\".<br>"
    "More information are available at the address:<br>"
    "<a href=\"https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm\">https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm"
    );
    QLabel *labelEntrate = new QLabel(introEntrate, this);

    buttonEntratePa = new QPushButton(tr("Download for fatturaPA"), this);
    connect(buttonEntratePa, &QPushButton::clicked,
            this, &DialogDownloadXsl::onButtonEntratePaClicked);

    buttonEntrateOrdinaria = new QPushButton(tr("Download for Fattura Ordinaria"), this);
    connect(buttonEntrateOrdinaria, &QPushButton::clicked,
            this, &DialogDownloadXsl::onButtonEntrateOrdinariaClicked);

    QHBoxLayout *entrateLayout= new QHBoxLayout;
    entrateLayout->addWidget(buttonEntratePa);
    entrateLayout->addWidget(buttonEntrateOrdinaria);

    /*
     * Progress bar and text log
     */
    progressBar = new QProgressBar(this);
    progressBar->setOrientation(Qt::Horizontal);
    progressBar->setMinimum(PHASE_IDLE);
    progressBar->setMaximum(PHASE_END);
    progressBar->setValue(PHASE_IDLE);

    textEditLog = new QTextEdit(this);
    textEditLog->setReadOnly(true);

    /*
     * Dialog button-box
     */
    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok |
        QDialogButtonBox::Cancel
    );
    connect(buttonBox, &QDialogButtonBox::accepted, this, &DialogDownloadXsl::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    /*
     * Main layout
     */
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(labelIntro);
    mainLayout->addWidget(labelAssoSoftware);
    mainLayout->addWidget(buttonAssoSoftware);
    mainLayout->addWidget(labelEntrate);
    mainLayout->addLayout(entrateLayout);
    mainLayout->addWidget(progressBar);
    mainLayout->addWidget(textEditLog);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
}

void DialogDownloadXsl::setStylesheets(QMap<QString, QString> &styleshets)
{
    this->stylesheets = styleshets;
}

/**
 * @brief Prints log messages and may re-enable the download buttons
 * @param message message string to print
 * @param type classificatoin of the message type
 *
 * This function does two things:
 * - formats the text log
 * - re-enables the download buttons if and error or a success message is print.
 */
void DialogDownloadXsl::printLog(const QString &message, MessageType type)
{
    QString messageFormat;

    if (type == ErrorMessage) {
        messageFormat = "style=\"color:red\"";
        downloadButtonSetEnabled(true);
    } else if (type == SuccessMessage) {
        messageFormat = "style=\"color:green\"";
        downloadButtonSetEnabled(true);
    }

    QString timestamp = QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    QString formattedMessage(message);
    formattedMessage.replace('\n', "<br>    ");
    formattedMessage = QString("<pre %1>%2| %3</pre>")
        .arg(messageFormat)
        .arg(timestamp)
        .arg(formattedMessage);
    textEditLog->append(formattedMessage);
}

/*!
 * @breif Starts the network request to download the stylesheet
 * @param url resource to download
 */
void DialogDownloadXsl::downloadUrl(const QUrl &url)
{
    // Clear previous log messages
    textEditLog->clear();
    progressBar->setValue(PHASE_IDLE);
    downloadButtonSetEnabled(false);
    timerTimeout->start();

    manager = new QNetworkAccessManager(this);
    // Allow to follow redirects
    // TODO: is this safe? better ask the user?
    manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    connect(manager, &QNetworkAccessManager::finished,
            this, &DialogDownloadXsl::onDownloadFinished);
    networkReply = manager->get(QNetworkRequest(url));
    connect(networkReply, &QNetworkReply::redirected,
            this, &DialogDownloadXsl::onRequestRedirected);

    printLog(tr("Requesting URL\n%1").arg(url.toString()));
    progressBar->setValue(PHASE_NET_REQUEST_DONE);
}

/**
 * @brief Notify the user we are following a redirected
 */
void DialogDownloadXsl::onRequestRedirected(const QUrl& url)
{
    printLog(tr("HTTP request redirected to \"%1\"").arg(url.toString()));
}

/**
 * @brief Slot called when QNetworkManager finished the network request
 */
void DialogDownloadXsl::onDownloadFinished(QNetworkReply *reply)
{
    timerTimeout->stop();
    QUrl url = reply->url();

    if (reply->error()) {
        printLog(tr("ERROR: download of\n\"%1\"\nfailed with the following cause:\n%2")
            .arg(url.toString())
            .arg(reply->errorString()), ErrorMessage);
        progressBar->setValue(PHASE_END);
    } else {
        printLog(tr("Network request success"));
        progressBar->setValue(PHASE_GOT_NETWORK_RESPONSE);

        QFileInfo info(url.fileName());
        QString suffix = info.suffix().toUpper();

        if (suffix == "ZIP") {
            unzipFile(reply);
        } else if (suffix == "XSL") {
            saveFile(reply, url.fileName());
        } else {
            printLog(tr("ERROR: %1 is not a supported format").arg(suffix), ErrorMessage);
            progressBar->setValue(PHASE_END);
        }
    }

    // It is my responsability to free the QNetworkReply.
    // onDownloadFinished is called even when i cancel the request on timeout
    reply->deleteLater();
}

/**
 * @brief Save the requested network resource when it is a single XSL file
 */
void DialogDownloadXsl::saveFile(QIODevice *device, const QString &filename)
{
    QString dataDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString filePath = dataDir + QDir::separator() + filename;
    QFile file(filePath);

    if (!file.open(QIODevice::WriteOnly)) {
        printLog(tr("ERROR: could not open\n\"%1\" for writing:\n%2")
            .arg(filePath)
            .arg(file.errorString()), ErrorMessage);
        progressBar->setValue(PHASE_END);
        return;
    }

    file.write(device->readAll());
    file.close();
    printLog(tr("File saved as:\n%1").arg(filePath));
    registerXsl(filename, filePath);
}

/**
 * @brief Extracts the requested network resource if it is a compressed package
 */
void DialogDownloadXsl::unzipFile(QIODevice *device)
{
    char* replyBuffer;
    qint64 bufferSize;
    qint64 readSize;
    qint64 wroteSize;
    QStringList zippedFilesList;
    int zippedFilesCount;
    QFileInfo info;
    QString fileName;
    QString filePath;
    QString suffix;
    QString destinationPath;
    QString result;
    QString dataDir;
    QTemporaryFile tempFile(this);

    if (!tempFile.open()) {
        qWarning() << "Failed to open temp file";
    }
    bufferSize = device->bytesAvailable();
    if (bufferSize <= 0) {
        qWarning() << "Network reply empty";
    }
    replyBuffer = new char[bufferSize];
    readSize = device->read(replyBuffer, bufferSize);
    if (readSize < 0) {
        qWarning() << "Error reading from the network reply buffer";
    }
    wroteSize = tempFile.write(replyBuffer, bufferSize);
    if (wroteSize != readSize) {
        qWarning() << "Wrote" << wroteSize << "bytes instead of the" << readSize << "bytes read";
    }
    if (!tempFile.flush()) {
        qWarning() << "Temp file flush failed";
    }
    delete[] replyBuffer;

    qDebug() << "Temp zip file:" << tempFile.fileName();
    zippedFilesList = JlCompress::getFileList(&tempFile);
    zippedFilesCount = zippedFilesList.count();

    if (zippedFilesList.empty()) {
        qWarning() << "Failed to read the content of the zip package with quazip";
        printLog(tr("ERROR: failed to inspect the compressed file."), ErrorMessage);
        progressBar->setValue(PHASE_END);
        return;
    }

    if (zippedFilesCount != 1) {
        qDebug() << "The ZIP package contains" << zippedFilesCount << "files instead of the expected 1";
        printLog(tr("ERROR: the compressed file contains too many files."), ErrorMessage);
        progressBar->setValue(PHASE_END);
        return;
    }

    dataDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    for (int i = 0; i < zippedFilesCount; i++) {
        filePath = zippedFilesList.at(i);
        info = QFileInfo(filePath);
        suffix = info.suffix().toUpper();
        if (suffix == "XSL") {
            fileName = info.fileName();
            destinationPath = dataDir + QDir::separator() + fileName;
            printLog(tr("Extracting file \"%1\"").arg(filePath));
            result = JlCompress::extractFile(&tempFile, filePath, destinationPath);
            if (result.isEmpty()) {
                printLog(tr("ERROR: failed to extract \"%1\"").arg(fileName), ErrorMessage);
                progressBar->setValue(PHASE_END);
            } else {
                printLog(tr("File extracted in\n%1").arg(result));
                registerXsl(fileName, destinationPath);
            }
        }
    }
}

/**
 * @brief Register the saved XSL file in the QMap handling the conflicts
 */
void DialogDownloadXsl::registerXsl(const QString &name, const QString &path)
{
    QString message;
    QString confirmedName = name;
    bool isOk = true;

    progressBar->setValue(PHASE_REGISTERING_XSL);
    if (stylesheets.contains(name)) {
        // Solve conflict
        message = tr("A stylesheet with the following name is already present for the file\n%1.\n"
            "You can:\n"
            "- change the name and create a new entry;\n"
            "- confirm the current name and overwrite the existing entry.")
            .arg(stylesheets[name]);

        confirmedName = QInputDialog::getText(this,
            tr("Name already in use"),
            message,
            QLineEdit::Normal,
            name,
            &isOk);
    }

    if (isOk) {
        stylesheets[confirmedName] = path;
        message = tr("Successfully downloaded and registered the stylesheet\n%1\nwith the name \"%2\"")
            .arg(path)
            .arg(confirmedName);
        printLog(message, SuccessMessage);
    } else {
        printLog(tr("Stylesheet registering cancelled by the user"));
    }
    progressBar->setValue(PHASE_END);
}

/**
 * @brief Gets the url for the named resource passing through the QSettings
 * @param name name of the resource to get
 * @param default url for the resource
 * @return the QUrl of the resource requested
 *
 * There url is first read from the settings, If there is not an entry in the
 * settings then the default value is used and a new entry is saved in the
 * settings. This way the URLs are not only hard-coded in the executable.
 */
const QUrl DialogDownloadXsl::getUrl(const QString &name, const QString &defaultUrl)
{
    QSettings settings;
    QUrl url;
    url = settings.value(name, QUrl(defaultUrl)).toUrl();
    if (!settings.contains(name)) {
            settings.setValue(name, url);
    }
    return url;
}

void DialogDownloadXsl::downloadButtonSetEnabled(bool status)
{
    buttonAssoSoftware->setEnabled(status);
    buttonEntrateOrdinaria->setEnabled(status);
    buttonEntratePa->setEnabled(status);
}

void DialogDownloadXsl::onButtonEntratePaClicked(bool checked)
{
    const QString name("download-xsl/fattura-pa");
    QUrl url;

    url = getUrl(name,
                 "https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Foglio_di_stile_fatturaPA_v1.2.1.xsl");
    downloadUrl(url);
}

void DialogDownloadXsl::onButtonEntrateOrdinariaClicked(bool checked)
{
    const QString name("download-xsl/fattura-ordinaria");
    QUrl url;

    url = getUrl(name,
                 "https://www.fatturapa.gov.it/export/documenti/fatturapa/v1.2.1/Foglio_di_stile_fatturaordinaria_v1.2.1.xsl");
    downloadUrl(url);
}

void DialogDownloadXsl::onButtonAssoSoftwareClicked(bool checked)
{
    const QString name("download-xsl/asso-software");
    QUrl url;

    url = getUrl(name,
                 "http://www.assosoftware.it/allegati/assoinvoice/FoglioStileAssoSoftware.zip");
    downloadUrl(url);
}

void DialogDownloadXsl::onRequestTimeout()
{
    printLog(tr("Network request interrupted because reached timout limit."), ErrorMessage);
    networkReply->close();
}
