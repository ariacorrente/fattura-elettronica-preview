/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DIALOGDOWNLOADXSL_H
#define DIALOGDOWNLOADXSL_H

#include <QDialog>
#include <QMap>

class QPushButton;
class QTextEdit;
class QProgressBar;
class QNetworkAccessManager;
class QNetworkReply;
class QTimer;

class DialogDownloadXsl : public QDialog
{
    Q_OBJECT

public:
    DialogDownloadXsl(QWidget *parent = nullptr);
    void setStylesheets(QMap<QString, QString> &styleshets);
    QMap<QString, QString> getstyleshets() const { return stylesheets; };

public slots:
    void onButtonEntratePaClicked(bool checked = true);
    void onButtonEntrateOrdinariaClicked(bool checked = true);
    void onButtonAssoSoftwareClicked(bool checked = true);
    void onDownloadFinished(QNetworkReply *reply);
    void onRequestTimeout();
    void onRequestRedirected(const QUrl& url);

private:
    /// Phases of the process of getting and saving the stylesheets
    enum PhaseProgressBar {
        PHASE_IDLE                 = 0,
        PHASE_NET_REQUEST_DONE     = 1,
        PHASE_GOT_NETWORK_RESPONSE = 2,
        PHASE_REGISTERING_XSL      = 3,
        PHASE_END                  = 4
    };
    /// Flags to identify the type of message to print in the textEditLog
    enum MessageType {
        StatusMessage,
        ErrorMessage,
        SuccessMessage
    };
    QMap<QString, QString> stylesheets;
    QPushButton *buttonAssoSoftware;
    QPushButton *buttonEntratePa;
    QPushButton *buttonEntrateOrdinaria;
    QTextEdit *textEditLog;
    QProgressBar *progressBar;
    QNetworkAccessManager *manager;
    QNetworkReply *networkReply;
    /// Timer to cancel the network request if it takes too long to complete
    QTimer *timerTimeout;
    void initGui();
    void printLog(const QString &message, MessageType type = StatusMessage);
    void downloadUrl(const QUrl &url);
    void saveFile(QIODevice *device, const QString &filename);
    void unzipFile(QIODevice *device);
    void registerXsl(const QString &name, const QString &path);
    const QUrl getUrl(const QString &name, const QString &defaultUrl);
    void downloadButtonSetEnabled(bool status);
};

#endif // DIALOGDOWNLOADXSL_H
