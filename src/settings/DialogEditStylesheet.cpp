/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DialogEditStylesheet.h"

#include <QPushButton>
#include <QLineEdit>
#include <QBoxLayout>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

DialogEditStylesheet::DialogEditStylesheet(QDialog *parent)
: QDialog(parent)
{
    initGui();
}

void DialogEditStylesheet::initGui()
{
    lineEditName = new QLineEdit(this);
    lineEditPath = new QLineEdit(this);

    QPushButton *buttonBrowserFile = new QPushButton(tr("Browse"), this);
    connect(buttonBrowserFile, &QPushButton::clicked,
            this, &DialogEditStylesheet::onButtonBrowseClicked);

    QHBoxLayout *layoutPath = new QHBoxLayout;
    layoutPath->addWidget(lineEditPath, 10);
    layoutPath->addWidget(buttonBrowserFile);

    /*
     * Dialog button-box
     */
    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok |
        QDialogButtonBox::Cancel
    );
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    /*
     * Main layout
     */
    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->addRow(tr("Name:"), lineEditName);
    mainLayout->addRow(tr("Path:"), layoutPath);
    mainLayout->addRow(buttonBox);
    setLayout(mainLayout);
}

/**
 * @brief Load data required from the dialog to work
 *
 * @param stylesheets: QMap of names/paths of all the stylesheets saved
 * @param name: name of the stylesheet to edit
 * @param path: path of the stylesheet to edit
 *
 * `stylesheets` is required to check for duplicate names.
 *
 * `name` and `path` is loaded into the QLineEdits when the dialog is used to
 * edit a stylesheet entry.
 */
void DialogEditStylesheet::loadStylesheets(
    QMap<QString, QString> &stylesheets,
    const QString &name,
    const QString &path)
{
    this->stylesheets = stylesheets;
    this->name = name;
    this->path = path;
    lineEditName->setText(name);
    lineEditPath->setText(path);
}

/**
 * @brief Check for duplicate names before closing.
 */
void DialogEditStylesheet::accept()
{
    QString proposedName = lineEditName->text();
    // The only case when contains() can return true is when the name is
    // not changed
    if (stylesheets.contains(proposedName) && proposedName != name) {
        QString message;
        message = tr(
            "The name \"%1\" is already in use for the stylesheet \"%2\".\n\n"
            "You need to change the new name to an unique one.")
            .arg(proposedName)
            .arg(stylesheets[proposedName]);
        QMessageBox::warning(this, tr("Name already in use"),message);
        lineEditName->selectAll();
    } else {
        name = lineEditName->text();
        path = lineEditPath->text();
        QDialog::accept();
    }
}

void DialogEditStylesheet::onButtonBrowseClicked(bool checked)
{
    QString folder;
    QString filePath;

    folder = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
    filePath = QFileDialog::getOpenFileName(this,
        tr("XSL Stylesheets"),
        folder,
        tr("XSL files (*.xsl)"));

    if (!filePath.isEmpty()) {
        lineEditPath->setText(filePath);

        // If name is empty propose a name, the name of the XSL file
        if (lineEditName->text().isEmpty()) {
            QString fileName;
            QFileInfo fileInfo(filePath);
            fileName = fileInfo.fileName();
            lineEditName->setText(fileName);
        }
    }
}

/**
 * @brief The dialog must be wide enough to display the XSL path.
 */
QSize DialogEditStylesheet::sizeHint() const
{
    int width = this->parentWidget()->size().width() * 2;
    return QSize(width, QDialog::sizeHint().height());
}
