/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DIALOGEDITSTYLESHEET_H
#define DIALOGEDITSTYLESHEET_H

#include <QDialog>
#include <QMap>

class QLineEdit;
class QString;

class DialogEditStylesheet : public QDialog
{

    Q_OBJECT

public:
    DialogEditStylesheet(QDialog *parent = nullptr);
    void loadStylesheets(QMap<QString, QString> &stylesheets,
                         const QString &name = QString(),
                         const QString &path = QString());
    void accept() override;
    const QString& getName() const { return name; };
    const QString& getPath() const { return path; };
    QSize sizeHint() const override;

public slots:
    void onButtonBrowseClicked(bool checked = true);

private:
    QString name;
    QString path;
    QMap<QString, QString> stylesheets;
    QLineEdit *lineEditPath;
    QLineEdit *lineEditName;
    void initGui();
};

#endif // DIALOGEDITSTYLESHEET_H
