/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DialogSettings.h"

#include "DefaultSettings.h"
#include "DialogEditStylesheet.h"
#ifdef USE_DOWNLOAD_TOOL
    #include "DialogDownloadXsl.h"
#endif // USE_DOWNLOAD_TOOL

#include <QCheckBox>
#include <QBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QListWidget>
#include <QComboBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QSettings>
#include <QDialogButtonBox>
#include <QLabel>
#include <QListView>
#include <QDebug>
#include <QMessageBox>
#include <QGroupBox>
#include <QTranslator>
#include <QLineEdit>
#include <QTextEdit>

DialogSettings::DialogSettings(QWidget *parent)
: QDialog(parent)
{
    QString name;
    QString path;

    settings = new QSettings(this);

    // Load the name and path info for the stylesheets into a QMap
    int size = settings->beginReadArray("stylesheets");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        name = settings->value("name").toString();
        path = settings->value("path").toString();
        stylesheets[name] = path;
    }
    settings->endArray();

    initGui();
}

void DialogSettings::initGui()
{
    setWindowTitle(tr("Settings"));

    /*
     * Load settings in list widget
     */
    listWidgetXsl = new QListWidget(this);
    listWidgetXsl->setSelectionMode(QAbstractItemView::SingleSelection);
    populateListWidget();

    /*
     * Buttons to handle list view
     */
    QPushButton *buttonAddXsl = new QPushButton(tr("Add"), this);
    connect(buttonAddXsl, &QPushButton::clicked,
            this, &DialogSettings::onButtonAddXslClicked);

    buttonEditXsl = new QPushButton(tr("Edit"), this);
    connect(buttonEditXsl, &QPushButton::clicked,
            this, &DialogSettings::onButtonEditXslClicked);

    buttonDeleteXsl = new QPushButton(tr("Delete"), this);
    connect(buttonDeleteXsl, &QPushButton::clicked,
            this, &DialogSettings::onButtonDeleteXslClicked);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(buttonAddXsl);
    buttonLayout->addWidget(buttonEditXsl);
    buttonLayout->addWidget(buttonDeleteXsl);

    /*
     * GroupBox for the list widget
     */
    QGroupBox *groupboxXsl = new QGroupBox(tr("Stylesheets"), this);
    QVBoxLayout *groupboxXslLayout = new QVBoxLayout;
    groupboxXslLayout->addLayout(buttonLayout);
    groupboxXslLayout->addWidget(listWidgetXsl);
    groupboxXsl->setLayout(groupboxXslLayout);

#ifdef USE_DOWNLOAD_TOOL
    QPushButton *buttonDownloadXsl = new QPushButton(tr("Download stylesheets"), this);
    connect(buttonDownloadXsl, &QPushButton::clicked,
            this, &DialogSettings::onButtonDownloadXslClicked);
#endif // USE_DOWNLOAD_TOOL

    /*
     * ComboBox of active XSL
     */
    comboboxDefaultXsl = new QComboBox(this);
    populateCombobox();

    comboboxLocale = new QComboBox(this);
    comboboxLocale->addItem(QLocale::languageToString(QLocale::English),
                            QLocale(QLocale::English));
    comboboxLocale->addItem(QLocale::languageToString(QLocale::Italian),
                            QLocale(QLocale::Italian));
    QLocale savedLocale = QLocale(
        settings->value("locale", DefaultSettings::locale).toString()
    );
    int currentIndex = comboboxLocale->findData(savedLocale);
    comboboxLocale->setCurrentIndex(currentIndex);
    connect(comboboxLocale, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &DialogSettings::onComboboxLocaleCurrentIndexChanged);

    /*
     * Checkbox for useExternalOpenssl
     */
    checkboxUseExternalOpenssl = new QCheckBox(tr("Use external OpenSSL executable"), this);
    checkboxUseExternalOpenssl->setChecked(
        settings->value("useExternalOpenssl",
                        DefaultSettings::useExternalOpenssl).toBool()
    );

    /*
     * OpenSSL executable path
     */
    lineEditOpensslPath = new QLineEdit(this);
    lineEditOpensslPath->setText(
        settings->value("openssl-path", DefaultSettings::opensslPath).toString()
    );
    QPushButton *buttonBrowseOpenssl = new QPushButton(tr("Browse"), this);
    connect(buttonBrowseOpenssl, &QPushButton::clicked,
            this, &DialogSettings::onButtonBrowseOpensslClicked);
    QHBoxLayout *opensslLayout = new QHBoxLayout;
    opensslLayout->addWidget( lineEditOpensslPath, 1);
    opensslLayout->addWidget(buttonBrowseOpenssl, 0);

    QGroupBox* groupboxOpenssl = new QGroupBox(tr("OpenSSL"), this);
    QVBoxLayout* layoutOpenssl = new QVBoxLayout;
    layoutOpenssl->addWidget(checkboxUseExternalOpenssl);
    layoutOpenssl->addWidget(new QLabel(tr("External OpenSSL path"), this));
    layoutOpenssl->addLayout(opensslLayout);
    groupboxOpenssl->setLayout(layoutOpenssl);

    /*
     * Strings to remove on normalization in pattern replacement
     */
    QGroupBox* groupboxPatternStrings = new QGroupBox(tr("Strings to skip in file names"), this);
    QBoxLayout* layoutPatternStrings = new QVBoxLayout;
    QLabel* labelPatternStrings = new QLabel(tr("Applies when normalizing the company name."));
    textEditUserSkipStrings = new QTextEdit(this);
    textEditUserSkipStrings->setAcceptRichText(false);
    textEditUserSkipStrings->setWhatsThis(
        tr("Strings listed here will be skipped when running the normalization "
           "of company names for the pattern replacement when saving the "
           "invoice or exporting to PDF.\n"
           "One string per line.\n"
           "Strings are case sensitive.")
    );
    QString normalizationStrings = settings->value(
        "userSkipStrings",
        DefaultSettings::userSkipStrings).toString();
    textEditUserSkipStrings->setText(normalizationStrings);

    layoutPatternStrings->addWidget(labelPatternStrings);
    layoutPatternStrings->addWidget(textEditUserSkipStrings);
    groupboxPatternStrings->setLayout(layoutPatternStrings);

    /*
     * Column 1
     */
    QFormLayout *layoutColumn1 = new QFormLayout;
    layoutColumn1->addRow(tr("Active stylesheet"), comboboxDefaultXsl);
    layoutColumn1->addRow(groupboxXsl);
#ifdef USE_DOWNLOAD_TOOL
    layoutColumn1->addRow(buttonDownloadXsl);
#endif // USE_DOWNLOAD_TOOL

    /*
     * Column 2
     */
    QFormLayout *layoutColumn2 = new QFormLayout;
    layoutColumn2->addRow(groupboxPatternStrings);
    layoutColumn2->addRow(groupboxOpenssl);
    layoutColumn2->addRow(tr("Language"), comboboxLocale);

    /*
     * Dialog button-box
     */
    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok |
        QDialogButtonBox::Cancel
    );
    connect(buttonBox, &QDialogButtonBox::accepted, this, &DialogSettings::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    /*
     * Main layout
     */
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addLayout(layoutColumn1, 0, 0);
    mainLayout->addLayout(layoutColumn2, 0, 1);
    mainLayout->addWidget(buttonBox, 1, 0, 1, 2);
    setLayout(mainLayout);
}

void DialogSettings::populateCombobox()
{
    QString xslName;
    QString activeXsl;

    comboboxDefaultXsl->clear();
    comboboxDefaultXsl->addItem(tr("Show XML"), "XML");

    activeXsl = settings->value("active-stylesheet").toString();

    // Get the items from the QListWidget because they are already sorted
    int count = listWidgetXsl->count();
    for(int i = 0; i < count; i++) {
        xslName = listWidgetXsl->item(i)->text();
        comboboxDefaultXsl->addItem(xslName, stylesheets[xslName]);
        if (stylesheets[xslName] == activeXsl) {
            // +1 because there is the preset "XML" entry
            comboboxDefaultXsl->setCurrentIndex(i + 1);
        }
    }
}

void DialogSettings::populateListWidget()
{
    listWidgetXsl->clear();
    QMap<QString, QString>::const_iterator iter = stylesheets.constBegin();
    while (iter != stylesheets.constEnd()) {
        listWidgetXsl->addItem(iter.key());
        ++iter;
    }
    listWidgetXsl->sortItems();
}

void DialogSettings::onButtonAddXslClicked(bool checked)
{
    DialogEditStylesheet *dialog = new DialogEditStylesheet(this);
    dialog->setWindowTitle(tr("Add stylesheet"));
    dialog->loadStylesheets(stylesheets);
    if (dialog->exec() == QDialog::Accepted) {
        QString name = dialog->getName();
        QString path = dialog->getPath();
        listWidgetXsl->addItem(name);
        stylesheets[name] = path;
        populateCombobox();
        listWidgetXsl->sortItems();
    }
}

void DialogSettings::onButtonEditXslClicked(bool checked)
{
    QString path;
    QString name;
    QListWidgetItem *currentItem = listWidgetXsl->currentItem();
    if (currentItem == nullptr) {
        // Row not selected
        return;
    }
    name = currentItem->text();
    path = stylesheets[name];

    DialogEditStylesheet *dialog = new DialogEditStylesheet(this);
    dialog->setWindowTitle(tr("Edit stylesheet"));
    dialog->loadStylesheets(stylesheets, name, path);

    if (dialog->exec() == QDialog::Accepted) {
        QString oldName = currentItem->text();
        name = dialog->getName();
        path = dialog->getPath();
        currentItem->setText(name);
        stylesheets.remove(oldName);
        stylesheets[name] = path;
        populateCombobox();
        listWidgetXsl->sortItems();
    }
}

void DialogSettings::onButtonDeleteXslClicked(bool checked)
{
    int currentRow;
    QString currentName;
    QString message;
    QMessageBox::StandardButton response;

    currentRow = listWidgetXsl->currentRow();
    if (currentRow == -1) {
        // Row not selected
        return;
    }
    currentName = listWidgetXsl->item(currentRow)->text();
    message = tr("Are you sure you want to remove the stylesheet \"%1\" from the list?")
        .arg(currentName);
    response = QMessageBox::question(this,
        tr("Confirm delete of stylesheet"),
        message);

    if (response == QMessageBox::Yes) {
        QString activeName = comboboxDefaultXsl->currentText();
        listWidgetXsl->takeItem(currentRow);
        stylesheets.remove(currentName);
        populateCombobox();
        if (currentName == activeName) {
            int fallbackIndex;
            // Use another XSL if available
            if (comboboxDefaultXsl->count() > 1) {
                fallbackIndex = 1;
            } else {
                // Position 0 is the "Show XML" option
                fallbackIndex = 0;
            }
            comboboxDefaultXsl->setCurrentIndex(fallbackIndex);
        }
    }
}

#ifdef USE_DOWNLOAD_TOOL
void DialogSettings::onButtonDownloadXslClicked(bool checked)
{
    DialogDownloadXsl *dialog = new DialogDownloadXsl(this);
    dialog->setStylesheets(stylesheets);

    if (dialog->exec() == QDialog::Accepted) {
        stylesheets = dialog->getstyleshets();
        populateCombobox();
        populateListWidget();
    }
}
#endif // USE_DOWNLOAD_TOOL

void DialogSettings::onButtonBrowseOpensslClicked(bool checked)
{
    // As starting folder use the folder of the current value
    QFileInfo oldFile(lineEditOpensslPath->text());
    QDir oldDir = oldFile.dir();

    QString path = QFileDialog::getOpenFileName(this,
        tr("Select OpenSSL executable"),
        oldDir.path()
    );
    if (!path.isEmpty()) {
        lineEditOpensslPath->setText(path);
    }
}

void DialogSettings::onComboboxLocaleCurrentIndexChanged(int index)
{
    QMessageBox::information(this,
        tr("Application restart required"),
        tr("To load the new language you need to restart the application."));
}

/**
 * @brief Save settings before closing the settings dialog
 */
void DialogSettings::accept()
{
    QString name;
    int count;

    /*
     * Save the collection of stylesheets
     */
    count = listWidgetXsl->count();
    settings->beginWriteArray("stylesheets");
    // This remove() should delete all the existing saved stylesheets
    settings->remove("");
    for (int i = 0; i < count; ++i) {
        // Iterate the QListWidget to get the names sorted alphabetically
        // and get the paths from the QMap
        settings->setArrayIndex(i);
        name = listWidgetXsl->item(i)->text();
        settings->setValue("name", name);
        settings->setValue("path", stylesheets[name]);
    }
    settings->endArray();

    /*
     * Save active stylesheet
     */
    QString currentXslPath = comboboxDefaultXsl->currentData().toString();
    settings->setValue("active-stylesheet", currentXslPath);

    settings->setValue("useExternalOpenssl", checkboxUseExternalOpenssl->isChecked());

    /*
     * Save the path of the OpenSSL executable
     */
     settings->setValue("openssl-path", lineEditOpensslPath->text());

    /*
     * Save the locale
     * I could save directly a QLocale but it will be saved in a unreadable form
     * in the configuration files. I prefer to save the name of the locale, for
     * example "en_US".
     */
    settings->setValue("locale", comboboxLocale->currentData().toLocale().name());

    /*
     * Save normalization strings
     */
    settings->setValue("userSkipStrings",
                       textEditUserSkipStrings->toPlainText());

    QDialog::accept();
}
