/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2021 Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DIALOGSETTINGS_H
#define DIALOGSETTINGS_H

#include <QDialog>
#include <QMap>

class QCheckBox;
class QComboBox;
class QListWidget;
class QListWidgetItem;
class QSettings;
class QLineEdit;
class QTextEdit;

class DialogSettings : public QDialog
{

    Q_OBJECT

public:
    DialogSettings(QWidget *parent = nullptr);

public slots:
    void onButtonAddXslClicked(bool checked = true);
    void onButtonEditXslClicked(bool checked = true);
    void onButtonDeleteXslClicked(bool checked = true);
#ifdef USE_DOWNLOAD_TOOL
    void onButtonDownloadXslClicked(bool checked = true);
#endif // USE_DOWNLOAD_TOOL
    void accept() override;
    void onComboboxLocaleCurrentIndexChanged(int index);
    void onButtonBrowseOpensslClicked(bool checked = true);

private:
    // Key = stylesheet name; Value = stylesheet path
    QMap<QString, QString> stylesheets;
    QSettings *settings;
    QComboBox *comboboxDefaultXsl;
    QPushButton *buttonEditXsl;
    QPushButton *buttonDeleteXsl;
    QListWidget *listWidgetXsl;
    QComboBox *comboboxLocale;
    QCheckBox *checkboxUseExternalOpenssl;
    QLineEdit *lineEditOpensslPath;
    QTextEdit *textEditUserSkipStrings;
    void initGui();
    void populateCombobox();
    void populateListWidget();
};

#endif // DIALOGSETTINGS_H
