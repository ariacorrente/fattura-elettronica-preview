/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AttachmentWidget.h"

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QStandardPaths>
#include <QSettings>

AttachmentWidget::AttachmentWidget(const QString fileName,
                                   int invoiceIndex, int attachmentIndex,
                                   QWidget* parent)
: QWidget(parent)
{
    this->fileName = fileName;
    this->invoiceIndex = invoiceIndex;
    this->attachmentIndex = attachmentIndex;
    initGui();
}

void AttachmentWidget::initGui()
{
    QLabel *labelName = new QLabel(tr("Attachment name\n%1").arg(fileName));
    QHBoxLayout *layout = new QHBoxLayout();

    QPushButton *buttonSave = new QPushButton(tr("Save"), this);
    connect(buttonSave, &QPushButton::clicked,
        this, &AttachmentWidget::save);

    QPushButton *buttonOpen = new QPushButton(tr("Open"), this);
    connect(buttonOpen, &QPushButton::clicked,
        this, &AttachmentWidget::open);

    QHBoxLayout *layoutButtons = new QHBoxLayout;
    layoutButtons->addWidget(buttonOpen);
    layoutButtons->addWidget(buttonSave);

    layout->addWidget(labelName);
    layout->addLayout(layoutButtons);
    setLayout(layout);
}

void AttachmentWidget::open()
{
    emit openRequested(invoiceIndex, attachmentIndex);
}

void AttachmentWidget::save()
{
    emit saveRequested(invoiceIndex, attachmentIndex);
}
