/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ATTACHMENTWIDGET_H
#define ATTACHMENTWIDGET_H

#include <QWidget>

class AttachmentWidget : public QWidget
{
    Q_OBJECT

public:
    AttachmentWidget(const QString fileName,
                     int invoiceIndex,
                     int attachmentIndex,
                     QWidget *parent = nullptr);
    void initGui();

private slots:
    void open();
    void save();

signals:
    void saveRequested(int invoiceIndex, int attachmentIndex);
    void openRequested(int invoiceIndex, int attachmentIndex);

private:
    int invoiceIndex;
    int attachmentIndex;
    QString fileName;
};

#endif // ATTACHMENTWIDGET_H
