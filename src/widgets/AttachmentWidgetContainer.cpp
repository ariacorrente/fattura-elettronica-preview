/*
 * Raster To Vector Tool
 * Copyright (C) 2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AttachmentWidgetContainer.h"

#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>

AttachmentWidgetContainer::AttachmentWidgetContainer(QWidget *parent)
: QWidget(parent)
{
    initGui();
}

/*
 * Structure of the layout:
 *
 * QVBoxLayout
 * |
 * +-> QFormlayout
 * |   |
 * !   +-> Form Layout row 1
 * |   |   |
 * |   |   +-> QLabel with "Invoice number xyz"
 * |   |   |
 * |   |   +-> QAttachmentWidget
 * |   |
 * !   +-> Form Layout row 2
 * |       |
 * |       +-> Empty QLabel (to avoid repeating the invoice number every line)
 * |       |
 * |       +-> QAttachmentWidget
 * |
 * +-> QFormlayout
 *     +-> QLabel with "Invoice number abc"
 *     |
 *     +-> QAttachmentWidget

 */
void AttachmentWidgetContainer::initGui()
{
    mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
}

void AttachmentWidgetContainer::addItem(QString invoiceNumber, QWidget* widget)
{
    QFormLayout *invoiceLayout;
    QLabel *label = new QLabel(tr("Invoice number\n%1").arg(invoiceNumber));
    if (layouts.contains(invoiceNumber))
    {
        invoiceLayout = layouts[invoiceNumber];
        invoiceLayout->addRow(new QLabel, widget);
    } else {
        invoiceLayout = new QFormLayout;
        invoiceLayout->addRow(label, widget);
        mainLayout->addLayout(invoiceLayout);
        layouts[invoiceNumber] = invoiceLayout;
    }
}

void AttachmentWidgetContainer::clear()
{
    while (mainLayout->count() > 0) {
        QLayoutItem *layoutItem = mainLayout->takeAt(0);
        QFormLayout *child = static_cast<QFormLayout *>(layoutItem);
        while (child->count() > 0) {
            /*
             * QFormLayout::RemoveRow() will delete the pointers, from the
             * documentation:
             * https://doc.qt.io/qt-5/qformlayout.html#removeRow-1
             * All widgets and nested layouts that occupied this row are deleted
             * No need to delete the QLabel or the AttachmentWidget
             */
            child->removeRow(0);
        }
        delete child;
    }
    layouts.clear();
}
