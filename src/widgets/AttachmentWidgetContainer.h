/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ATTACHMENTWIDGETCONTAINER_H
#define ATTACHMENTWIDGETCONTAINER_H

#include <QWidget>
#include <QMap>

class QVBoxLayout;
class QFormLayout;

class AttachmentWidgetContainer : public QWidget
{

    Q_OBJECT

public:
    AttachmentWidgetContainer(QWidget *parent = nullptr);
    void addItem(QString invoiceNumber, QWidget *widget);
    void clear();

private:
    QVBoxLayout *mainLayout;
    /*
     * key: number of the invoice
     * value: layout with a collection of AttachmentWidget
     */
    QMap <QString, QFormLayout*> layouts;

    void initGui();
};

#endif // ATTACHMENTWIDGETCONTAINER_H
