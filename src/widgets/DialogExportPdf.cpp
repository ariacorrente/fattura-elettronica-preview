/*
 * Fattura Elettronica Preview
 * Copyright (C) 2019  Nicola Felice <nicola@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DialogExportPdf.h"

#include "PatternReplacerWidget.h"
#include "../fattura-elettronica/FatturaElettronica.h"
#include "../settings/DefaultSettings.h"

#include <QDialogButtonBox>
#include <QFileDialog>
#include <QMessageBox>
#include <QPageLayout>
#include <QDesktopServices>
#include <QSettings>
#include <QVBoxLayout>
#include <QWebEnginePage>

DialogExportPdf::DialogExportPdf(QWidget *parent)
: QDialog(parent)
, fe(nullptr)
, page(nullptr)
{
    settings = new QSettings(this);
    initGui();
}

void DialogExportPdf::initGui()
{
    setWindowTitle(tr("Export PDF"));
    patternWidget = new PatternReplacerWidget(this);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    buttonBox->addButton(QDialogButtonBox::Cancel);
    buttonBox->addButton(QDialogButtonBox::Save);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &DialogExportPdf::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(patternWidget);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
}

void DialogExportPdf::accept()
{
    assert(page != nullptr);

    // The proposed file name cames from the PatternWidget only if the file is
    // an invoice so the wigdget is enabled.
    QString proposedFileName;
    if (patternWidget->isEnabled()) {
        proposedFileName = patternWidget->getOutputString();
    } else {
        QFileInfo info(originalFileName);
        proposedFileName = info.path() + QDir::separator() + info.completeBaseName() + ".pdf";
    }

    QString fileName = QFileDialog::getSaveFileName(
        this,
        tr("Export to PDF"),
        proposedFileName,
        tr("PDF documents (*.pdf);;All files (*.*)")
    );

    if (!fileName.isEmpty()) {
        QPageLayout pageLayout(
            QPageSize(QPageSize::A4),
            QPageLayout::Portrait,
            // Margins in mm in the order: left, top, right and bottom
            QMarginsF(40, 40, 40, 40)
        );
        page->printToPdf(fileName, pageLayout);
    }
    // The dialog will be accepted only in onPdfPrintFinished because we need to
    // check if the PDF export was successfull
}

void DialogExportPdf::onPdfPrintFinished(const QString &filePath, bool success)
{
    if (success) {
        QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
        settings->setValue("patternExportPdf", patternWidget->getPattern());
        QDialog::accept();
    } else {
        QMessageBox::warning(
            this,
            tr("PDF export failed"),
            tr("Failed to export PDF to file '%1'").arg(filePath),
            QMessageBox::Ok
        );
    }
}

void DialogExportPdf::loadInvoice(const FatturaElettronica *invoice, const QString &fileName)
{
    originalFileName = fileName;
    QString pattern = settings->value("patternExportPdf", DefaultSettings::patternExportPdf).toString();
    if (invoice != nullptr) {
        patternWidget->setEnabled(true);
        patternWidget->setData(pattern, fileName, invoice);
    } else {
        patternWidget->setEnabled(false);
    }
}

void DialogExportPdf::loadPage(QWebEnginePage* page)
{
    this->page = page;
    connect(page, &QWebEnginePage::pdfPrintingFinished,
        this, &DialogExportPdf::onPdfPrintFinished);
}
