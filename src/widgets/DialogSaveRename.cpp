/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DialogSaveRename.h"

#include "PatternReplacerWidget.h"
#include "../fattura-elettronica/FatturaElettronica.h"
#include "../settings/DefaultSettings.h"

#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QFileDialog>
#include <QPushButton>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#include <QSettings>

DialogSaveRename::DialogSaveRename(QWidget* parent)
: QDialog(parent)
{
    settings = new QSettings(this);
    initGui();
}

void DialogSaveRename::initGui()
{
    setWindowTitle(tr("Save/Rename invoice"));

    patternWidget = new PatternReplacerWidget(this);

    checkBoxRename = new QCheckBox(tr("Rename"), this);
    checkBoxRename->setChecked(settings->value("renameEnabled", DefaultSettings::renameEnabled).toBool());
    checkBoxRename->setToolTip(tr("SdI will not accept invoice files with names not following the standard."));
    connect(checkBoxRename, &QCheckBox::stateChanged,
            this, &DialogSaveRename::onCheckBoxRenameStateChanged);

    /**
     * Form Layout
     */
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow(patternWidget);
    formLayout->addRow(checkBoxRename);

    /*
     * Dialog button-box
     */
    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    buttonBox->addButton(QDialogButtonBox::Cancel);
    buttonAccept = buttonBox->addButton(QDialogButtonBox::Save);
    buttonAccept->setText(tr("Save as..."));
    connect(buttonBox, &QDialogButtonBox::accepted, this, &DialogSaveRename::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(formLayout);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
}

void DialogSaveRename::accept()
{
    bool isSuccess = false;
    QFile file(originalFileName);

    if (!file.exists()) {
        QMessageBox::warning(
            this,
            tr("Unable to perform the operation"),
            tr("The file to be deleted does not exist.\n"
               "Possible causes are:\n"
               "- the file has already been renamed or deleted;\n"
               "- access permissions are not sufficient."),
            QMessageBox::Ok);
        return;
    }

    // The proposed file name cames from the PatternWidget only if the file is
    // an invoice so the wigdget is enabled.
    QString proposedFileName;
    if (patternWidget->isEnabled()) {
        proposedFileName = patternWidget->getOutputString();
    } else {
        proposedFileName = originalFileName;
    }

    QString fileName = QFileDialog::getSaveFileName(
        this,
        tr("Save/Move invoice"),
        proposedFileName,
        tr("Invoice documents (*.xml *.p7m);;All files (*.*)")
    );

    if (!fileName.isEmpty()) {
        QFile destinationFile(fileName);
        if (destinationFile.exists())  {
            isSuccess = destinationFile.remove();
            if (!isSuccess) {
                QMessageBox::warning(
                    this,
                    tr("Operation failed"),
                    tr("Overwrite operation failed.\n"
                        "Check access permission for the destination path."),
                    QMessageBox::Ok);
                return;
            }
        }

        if (checkBoxRename->isChecked()) {
            isSuccess = file.rename(fileName);
        } else {
            isSuccess = file.copy(fileName);
        }

        settings->setValue("patternSave", patternWidget->getPattern());
        settings->setValue("renameEnabled", checkBoxRename->isChecked());

        if (isSuccess == false) {
            QMessageBox::warning(this, tr("Operation failed"), tr("Save/Rename operation failed."));
        } else {
            QDialog::accept();
        }
    }
}

void DialogSaveRename::onCheckBoxRenameStateChanged(int state)
{
    if (state == Qt::Checked) {
        buttonAccept->setText(tr("Rename"));
    } else {
        buttonAccept->setText(tr("Save as..."));
    }
}

void DialogSaveRename::loadInvoice(const FatturaElettronica *invoice, const QString &fileName)
{
    originalFileName = fileName;
    QString pattern = settings->value("patternSave", DefaultSettings::patternSave).toString();
    if (invoice != nullptr) {
        patternWidget->setEnabled(true);
        patternWidget->setData(pattern, fileName, invoice);
    } else {
        patternWidget->setEnabled(false);
    }
}
