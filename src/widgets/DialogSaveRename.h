/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGSAVERENAME_H
#define DIALOGSAVERENAME_H

#include <QDialog>

class FatturaElettronica;
class PatternReplacerWidget;

class QCheckBox;
class QSettings;

class DialogSaveRename : public QDialog
{

    Q_OBJECT

public:
    DialogSaveRename(QWidget *parent = nullptr);
    void loadInvoice(const FatturaElettronica *invoice, const QString &fileName);

public slots:
    void accept() override;
    void onCheckBoxRenameStateChanged(int state);

private:
    FatturaElettronica *fe;
    PatternReplacerWidget *patternWidget;
    QCheckBox *checkBoxRename;
    QPushButton *buttonAccept;
    QSettings *settings;
    QString originalFileName;

    void initGui();
};

#endif // DIALOGSAVERENAME_H
