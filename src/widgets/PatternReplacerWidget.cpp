/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PatternReplacerWidget.h"
#include "../fattura-elettronica/FatturaElettronica.h"
#include "../settings/DefaultSettings.h"

#include <QComboBox>
#include <QDir>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QVector>

PatternReplacerWidget::PatternReplacerWidget(QWidget* parent)
: QWidget(parent)
, invoiceIndex(0)
, dialogHelp(nullptr)
{
    settings = new QSettings(this);
    initGui();
}

void PatternReplacerWidget::initGui()
{
    labelOriginalFileName = new QLabel(this);
    lineEditDestinationFilename = new QLineEdit(this);
    lineEditPattern = new QLineEdit(this);
    comboboxInvoiceNumber = new QComboBox(this);

    /*
     * Buttons layout
     */
    QPushButton *buttonRefresh = new QPushButton(tr("Refresh"), this);
    connect(buttonRefresh, &QPushButton::clicked,
            this, &PatternReplacerWidget::onButtonRefreshClicked);

    QPushButton *buttonHelp = new QPushButton(tr("Help"), this);
    connect(buttonHelp, &QPushButton::clicked,
            this, &PatternReplacerWidget::onHelpRequested);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addWidget(buttonHelp);
    buttonsLayout->addWidget(buttonRefresh);

    /*
     * Form Layout
     */
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow(tr("Pattern:"), lineEditPattern);
    formLayout->addRow(tr("Invoice number:"), comboboxInvoiceNumber);
    formLayout->addRow(tr("Original path:"), labelOriginalFileName);
    formLayout->addRow(tr("Destination path:"), lineEditDestinationFilename);
    formLayout->addRow(buttonsLayout);

    setLayout(formLayout);
}

void PatternReplacerWidget::setData(const QString &pattern,
                                    const QString &fileName,
                                    const FatturaElettronica *invoice)
{
    loadCombobox(invoice);
    lineEditPattern->setText(pattern);
    labelOriginalFileName->setText(QDir::toNativeSeparators(fileName));
    replacer.setData(invoice, invoiceIndex, fileName);

    QVariant value = settings->value("userSkipStrings", DefaultSettings::userSkipStrings);
    QStringList strings = value.toString().split("\n");
    replacer.setUserSkipStrings(strings);

    runReplacer();
}

void PatternReplacerWidget::setPattern(const QString& pattern)
{
    lineEditPattern->setText(pattern);
}

QString PatternReplacerWidget::getOutputString() const
{
    return lineEditDestinationFilename->text();
}

QString PatternReplacerWidget::getPattern() const
{
    return lineEditPattern->text();
}

void PatternReplacerWidget::loadCombobox(const FatturaElettronica *invoice)
{
    int index = 0;
    comboboxInvoiceNumber->clear();
    QVector<FatturaElettronicaBody*>::const_iterator iter = invoice->bodies.constBegin();
    while (iter != invoice->bodies.constEnd()) {
        comboboxInvoiceNumber->addItem((*iter)->datiGenerali.datiGeneraliDocumento.numero, index);
        ++iter;
        ++index;
    }
}

void PatternReplacerWidget::onButtonRefreshClicked(bool checked)
{
    runReplacer();
}

void PatternReplacerWidget::onHelpRequested()
{
    if (dialogHelp == nullptr) {
        dialogHelp = new QMessageBox(
            QMessageBox::Information,
            tr("Help on replacement labels"),
            tr(
"<p>The 'Destination path' is calculated starting from the 'Pattern' and \
replacing the labels starting with '%' with the corresponding data.</p>\n\
<p>Labels used in the pattern:</p>\
<ul>\
<li>%F: folder of source file;</li>\
<li>%f: base name of the file, without folder and extension;</li>\
<li>%x: extension of source file (xml or xml.p7m);</li>\
<li>%d: complete date of invoice<br/>\
    For the elements of the date:\
    <ul>\
        <li>%D: day;</li>\
        <li>%M: month;</li>\
        <li>%Y: year;</li>\
    </ul>\
</li>\
<li>%n: number of invoice;</li>\
<li>%a: name of cedente/prestatore;</li>\
<li>%A: normalized name of cedente/prestatore;</li>\
<li>%b: name of cessionario/committente;</li>\
<li>%B: normalized name of cessionario/committente;</li>\
<li>%s: path separator.</li>\
</ul>\
<p>If the opened file is a collection of invoices you may need to select the \
exact invoice you want to read data from.</p>"
            ),
            QMessageBox::Ok,
            this
        );
        dialogHelp->setWindowModality(Qt::NonModal);
        dialogHelp->setTextInteractionFlags(Qt::TextSelectableByMouse);
    }
    dialogHelp->show();
}

void PatternReplacerWidget::runReplacer()
{
    replacer.runPattern(lineEditPattern->text());
    QString outputString = replacer.getOutputString();
    outputString = QDir::toNativeSeparators(outputString);
    lineEditDestinationFilename->setText(outputString);
}
