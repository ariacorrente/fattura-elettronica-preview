/*
 * Fattura Elettronica Preview
 * Copyright (C) 2018-2019  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PATTERNREPLACERWIDGET_H
#define PATTERNREPLACERWIDGET_H

#include "../PatternReplacer.h"

#include <QWidget>

class FatturaElettronica;

class QComboBox;
class QLabel;
class QLineEdit;
class QMessageBox;
class QSettings;

class PatternReplacerWidget : public QWidget
{
    Q_OBJECT

public:
    PatternReplacerWidget(QWidget *parent = nullptr);
    QString getOutputString() const;
    QString getPattern() const;
    void setData(const QString &pattern,
                 const QString &fileName,
                 const FatturaElettronica *invoice);
    void setPattern(const QString &pattern);
    void setInvoiceIndex(int index);

public slots:
    void onButtonRefreshClicked(bool checked = false);
    void onHelpRequested();

private:
    int invoiceIndex;
    PatternReplacer replacer;
    QComboBox *comboboxInvoiceNumber;
    QLabel *labelOriginalFileName;
    QLineEdit *lineEditDestinationFilename;
    QLineEdit *lineEditPattern;
    QMessageBox *dialogHelp;
    QSettings *settings;

    void initGui();
    void runReplacer();
    void loadCombobox(const FatturaElettronica *invoice);
};

#endif // PATTERNREPLACERWIDGET_H
