/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Nicola Felice <nicola@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestPatternReplacer.h"

#include "../src/fattura-elettronica/FatturaElettronica.h"
#include "../src/PatternReplacer.h"

#include <QTest>

QTEST_APPLESS_MAIN(TestPatternReplacer);

void TestPatternReplacer::initTestCase()
{
    // Called before the first testfunction is executed
}

void TestPatternReplacer::cleanupTestCase()
{
    // Called after the last testfunction was executed
}

void TestPatternReplacer::init()
{
    // Called before every testfunction
}

void TestPatternReplacer::cleanup()
{
    // Called after every testfunction
}

void TestPatternReplacer::testInvoices_data()
{
    FatturaElettronica invoice;

    QTest::addColumn<FatturaElettronica>("invoice");
    QTest::addColumn<int>("invoiceIndex");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QString>("expected");

    QTest::newRow("Empty invoice") << invoice << 0 << "" << "";
    QTest::newRow("Negative InvoiceIndex") << invoice << -1 << "%n" << "";

    FatturaElettronicaBody *body1 = new FatturaElettronicaBody;
    invoice.bodies.append(body1);
    QTest::newRow("First invoice") << invoice << 0 << "" << "";

    FatturaElettronicaBody *body2 = new FatturaElettronicaBody;
    invoice.bodies.append(body2);
    QTest::newRow("Second invoice") << invoice << 1 << "" << "";
    QTest::newRow("InvoiceIndex > bodies count") << invoice << 2 << "%n" << "";

    QTest::newRow("Escape '%' alone") << invoice << 0 << "%%" << "%";
    QTest::newRow("Escape '%'") << invoice << 0 << "ab%%cd" << "ab%cd";
    QTest::newRow("Escape two '%'") << invoice << 0 << "ab%%%%cd" << "ab%%cd";
    QTest::newRow("Non existing token '%'") << invoice << 0 << "ab%cd" << "abd";
    QTest::newRow("Three '%'") << invoice << 0 << "ab%%%cd" << "ab%d";
    QTest::newRow("Start with '%'") << invoice << 0 << "%abcd" << "bcd";
    QTest::newRow("End with '%'") << invoice << 0 << "abcd%" << "abcd";
}

void TestPatternReplacer::testInvoices()
{
    QFETCH(FatturaElettronica, invoice);
    QFETCH(QString, pattern);
    QFETCH(QString, expected);

    const QString fileName = "/home/username/invoice.xml.p7m";
    const int invoiceIndex = 0;

    PatternReplacer replacer;
    QString output;

    replacer.setData(&invoice, invoiceIndex, fileName);
    replacer.runPattern(pattern);
    output = replacer.getOutputString();
    QCOMPARE(output, expected);
}

void TestPatternReplacer::testDate_data()
{
    QTest::addColumn<FatturaElettronica>("invoice");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QString>("expected");

    FatturaElettronicaBody *body = new FatturaElettronicaBody;
    body->datiGenerali.datiGeneraliDocumento.data = "2001-02-03";
    body->datiGenerali.datiGeneraliDocumento.numero = "123";
    FatturaElettronica *invoice = new FatturaElettronica();
    invoice->bodies.append(body);

    QTest::newRow("Empty string") << *invoice << "%d" << "2001-02-03";
    QTest::newRow("Inside string") << *invoice << "hello%dword" << "hello2001-02-03word";
    QTest::newRow("Before string") << *invoice << "%dword" << "2001-02-03word";
    QTest::newRow("After string") << *invoice << "hello%d" << "hello2001-02-03";
    QTest::newRow("Get year") << *invoice << "hello%Y" << "hello2001";
    QTest::newRow("Get month") << *invoice << "hello%M" << "hello02";
    QTest::newRow("Get day") << *invoice << "hello%D" << "hello03";
    QTest::newRow("Compose a date") << *invoice << "hello_%D-%M-%Y" << "hello_03-02-2001";
    invoice->bodies[0]->datiGenerali.datiGeneraliDocumento.data = "abcd";
    // No checks will be performed on malformed date
    QTest::newRow("%d with not a date") << *invoice << "hello%d" << "helloabcd";
    QTest::newRow("%Y with not a date") << *invoice << "hello%Y" << "hello";
    invoice->bodies[0]->datiGenerali.datiGeneraliDocumento.data = "--";
    QTest::newRow("%D with 2 dashes as date") << *invoice << "hello%D" << "hello";
}

void TestPatternReplacer::testDate()
{
    QFETCH(FatturaElettronica, invoice);
    QFETCH(QString, pattern);
    QFETCH(QString, expected);

    QString fileName = "/home/username/invoice.xml.p7m";
    int invoiceIndex = 0;

    PatternReplacer replacer;
    QString output;

    replacer.setData(&invoice, invoiceIndex, fileName);
    replacer.runPattern(pattern);
    output = replacer.getOutputString();
    QCOMPARE(output, expected);
}

void TestPatternReplacer::testFileName_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QString>("expected");

    QTest::newRow("Only pattern") << "" << "%f" << "";
    // TODO: test paths for other operating systems (if possible)
#ifdef Q_OS_UNIX
    QTest::newRow("Linux base name") << "/a/path.xml" << "%f" << "path";
    QTest::newRow("Linux folder") << "/a/path.xml" << "%F" << "/a";
    QTest::newRow("Linux suffix") << "/a/path.xml" << "%x" << "xml";
    QTest::newRow("Linux complete path") << "/a/path.xml" << "%F%s%f.%x" << "/a/path.xml";
    QTest::newRow("Use '/' instead of '%s'") << "/a/path.xml" << "%F/%f.%x" << "/a/path.xml";
#endif
#ifdef Q_OS_WIN
    QTest::newRow("Windows base name") << "c:\\a\\path.xml" << "%f" << "path";
#endif
}

void TestPatternReplacer::testFileName()
{
    QFETCH(QString, fileName);
    QFETCH(QString, pattern);
    QFETCH(QString, expected);

    FatturaElettronicaBody *body = new FatturaElettronicaBody;
    FatturaElettronica invoice;
    invoice.bodies.append(body);

    PatternReplacer replacer;
    QString output;

    replacer.setData(&invoice, 0, fileName);
    replacer.runPattern(pattern);
    output = replacer.getOutputString();
    QCOMPARE(output, expected);
}

void TestPatternReplacer::testDenominazione_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("pattern");
    QTest::addColumn<QString>("expected");

    QTest::addRow("Nothing to do") << "Company" << "%A" << "Company";
    QTest::addRow("S.r.l.") << "Company S.r.l." << "%A" << "Company";
    QTest::addRow("S.R.L.") << "Company S.R.L." << "%A" << "Company";
    QTest::addRow("s.r.l.") << "Company s.r.l." << "%A" << "Company";
    QTest::addRow("SRL") << "Company SRL" << "%A" << "Company";
    QTest::addRow("A space") << "Company Name" << "%A" << "Company-Name";
    QTest::addRow("Two spaces") << "Company  Name" << "%A" << "Company-Name";
    QTest::addRow("Regex with spaces") << "Company SOC. COOP. A.R.L." << "%A" << "Company";
    QTest::addRow("Avoid to replace leading SS") << "Companyss" << "%A" << "Companyss";
    QString withSeparator = QString("Company").append(QDir::separator()).append("Name");
    QTest::addRow("Dir separator") << withSeparator << "%A" << "Company-Name";
}

void TestPatternReplacer::testDenominazione()
{
    QFETCH(QString, name);
    QFETCH(QString, pattern);
    QFETCH(QString, expected);

    FatturaElettronica invoice;
    invoice.header.cedentePrestatore.datiAnagrafici.anagrafica.denominazione = name;
    QString fileName;
    PatternReplacer replacer;
    QString output;

    replacer.setData(&invoice, 0, fileName);
    replacer.runPattern(pattern);
    output = replacer.getOutputString();
    QCOMPARE(output, expected);
}

void TestPatternReplacer::testUseCase1()
{
    PatternReplacer replacer;
    QString pattern = "%F%s%Y%M%D_%f.%x";
    QString fileName = "/home/username/invoice.xml.p7m";
    FatturaElettronica invoice;
    FatturaElettronicaBody *body = new FatturaElettronicaBody();
    body->datiGenerali.datiGeneraliDocumento.data = "2001-02-03";
    body->datiGenerali.datiGeneraliDocumento.numero = "123";
    invoice.bodies.append(body);
    QString output;

    replacer.setData(&invoice, 0, fileName);
    replacer.runPattern(pattern);
    output = replacer.getOutputString();
    QCOMPARE(output, "/home/username/20010203_invoice.xml.p7m");
}
