/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  Nicola Felice <nicola@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTPATTERNREPLACER_H
#define TESTPATTERNREPLACER_H

#include <QObject>

class TestPatternReplacer : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    void testInvoices();
    void testInvoices_data();
    void testDate();
    void testDate_data();
    void testFileName();
    void testFileName_data();
    void testDenominazione();
    void testDenominazione_data();
    void testUseCase1();
};

#endif // TESTPATTERNREPLACER_H
