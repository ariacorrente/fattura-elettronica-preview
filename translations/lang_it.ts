<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>AttachmentWidget</name>
    <message>
        <location filename="../src/widgets/AttachmentWidget.cpp" line="41"/>
        <source>Attachment name
%1</source>
        <translation>Nome allegato
%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/AttachmentWidget.cpp" line="44"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../src/widgets/AttachmentWidget.cpp" line="48"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
</context>
<context>
    <name>AttachmentWidgetContainer</name>
    <message>
        <location filename="../src/widgets/AttachmentWidgetContainer.cpp" line="65"/>
        <source>Invoice number
%1</source>
        <translation>Numero fattura
%1</translation>
    </message>
</context>
<context>
    <name>DialogDownloadXsl</name>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="55"/>
        <source>Download stylesheets</source>
        <translation>Scarica fogli di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="61"/>
        <source>This tool allows to download some stylesheets available online.
These stylesheets are not packaged with this program because they are developed by others.
</source>
        <translation>Questo strumento permette di scaricare alcuni fogli di stile disponibili online.
Questi fogli di stile non sono integrati in questo programma perchè sono sviluppati da terze parti.
</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="70"/>
        <source>&lt;strong&gt;Stylesheet from AssoSoftware&lt;/strong&gt;&lt;br&gt;Before downloading and using this stylesheet you need to accept the license and the terms of use&lt;br&gt;available at this address: &lt;a href=&quot;http://www.assosoftware.it/assoinvoice&quot;&gt;http://www.assosoftware.it/assoinvoice&lt;/a&gt;.</source>
        <translation>&lt;strong&gt;Foglio di stile di AssoSoftware&lt;/strong&gt;&lt;br&gt;Prima di scaricare ed utilizzare questo foglio di stile è necessario accettare la licenza ed i termini d&apos;uso&lt;br&gt;disponibili a questo indirizzo: &lt;a href=&quot;http://www.assosoftware.it/assoinvoice&quot;&gt;http://www.assosoftware.it/assoinvoice&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="75"/>
        <source>Download from AssoSoftware</source>
        <translation>Scarica da AssoSoftware</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="82"/>
        <source>&lt;strong&gt;Stylesheets from &quot;Agenzia delle Entrate&quot;&lt;/strong&gt;&lt;br&gt;The italian &quot;Agenzia delle Entrate&quot; published two stylesheets:&lt;br&gt;- stylesheet for for invoices in the format &quot;pubblica amministrazione&quot;;&lt;br&gt;- stylesheet for invoices in format &quot;fatture ordinarie&quot;.&lt;br&gt;More information are available at the address:&lt;br&gt;&lt;a href=&quot;https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm&quot;&gt;https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm</source>
        <translation>&lt;strong&gt;Foglio di stile da &quot;Agenzia delle Entrate&quot;&lt;/strong&gt;&lt;br&gt;L&apos;Agenzia delle Entrate pubblica due fogli di stile:&lt;br&gt;- foglio di stiler per fatture in formato PA &quot;pubblica amministrazione&quot;;&lt;br&gt;- Foglio di stile in formato &quot;fatture ordinarie&quot;.&lt;br&gt;Maggiori informazioni sono disponibili all&apos;indirizzo:&lt;br&gt;&lt;a href=&quot;https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm&quot;&gt;https://www.fatturapa.gov.it/export/fatturazione/it/normativa/f-2.htm</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="91"/>
        <source>Download for fatturaPA</source>
        <translation>Scarica per fatturaPA</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="95"/>
        <source>Download for Fattura Ordinaria</source>
        <translation>Scarica per fattura ordinaria</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="198"/>
        <source>Requesting URL
%1</source>
        <translation>Richiesta URL
%1</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="207"/>
        <source>HTTP request redirected to &quot;%1&quot;</source>
        <translation>Richiesta HTTP rediretta verso &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="219"/>
        <source>ERROR: download of
&quot;%1&quot;
failed with the following cause:
%2</source>
        <translation>ERRORE: scaricamento di
&quot;%1&quot;
fallito per il seguente motivo:
%2</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="224"/>
        <source>Network request success</source>
        <translation>Richiesta di rete terminata con successo</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="235"/>
        <source>ERROR: %1 is not a supported format</source>
        <translation>ERRORE: %1 non è un formato supportato</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="255"/>
        <source>ERROR: could not open
&quot;%1&quot; for writing:
%2</source>
        <translation>ERRORE: impossibile aprire
&quot;%1&quot; in modalità scrittura:
%2</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="264"/>
        <source>File saved as:
%1</source>
        <translation>File salvato come:
%1</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="315"/>
        <source>ERROR: failed to inspect the compressed file.</source>
        <translation>ERRORE: fallita inspezione del file compresso.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="322"/>
        <source>ERROR: the compressed file contains too many files.</source>
        <translation>ERRORE: il file compresso contiene troppi file.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="335"/>
        <source>Extracting file &quot;%1&quot;</source>
        <translation>Estrazione del file &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="338"/>
        <source>ERROR: failed to extract &quot;%1&quot;</source>
        <translation>ERRORE: fallita estrazione del file &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="341"/>
        <source>File extracted in
%1</source>
        <translation>File estratto in
%1</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="360"/>
        <source>A stylesheet with the following name is already present for the file
%1.
You can:
- change the name and create a new entry;
- confirm the current name and overwrite the existing entry.</source>
        <translation>Un foglio di stile ocn il seguente nome è già presente per il file
%1.
É possibile:
- cambiare il nome e creare una nuova voce;
- confermare il nome attuale e sovrascrivere la voce esistente.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="367"/>
        <source>Name already in use</source>
        <translation>Nome già in uso</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="376"/>
        <source>Successfully downloaded and registered the stylesheet
%1
with the name &quot;%2&quot;</source>
        <translation>Foglio di stile scaricato e registrato con successo
%1
con il nome &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="381"/>
        <source>Stylesheet registering cancelled by the user</source>
        <translation>Registrazione del foglio di stile annullata dall&apos;utente</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogDownloadXsl.cpp" line="446"/>
        <source>Network request interrupted because reached timout limit.</source>
        <translation>Richiesta di rete interrotta perchè il tempo limite è scaduto.</translation>
    </message>
</context>
<context>
    <name>DialogEditStylesheet</name>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="41"/>
        <source>Browse</source>
        <translation>Sfoglia</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="63"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="64"/>
        <source>Path:</source>
        <translation>Percorso:</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="103"/>
        <source>The name &quot;%1&quot; is already in use for the stylesheet &quot;%2&quot;.

You need to change the new name to an unique one.</source>
        <translation>Il nome &quot;%1&quot; è già in uso per lo stile &quot;%2&quot;.

E&apos; necessario utilizzare un altro nome che non sia già in uso.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="108"/>
        <source>Name already in use</source>
        <translation>Nome già in uso</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="124"/>
        <source>XSL Stylesheets</source>
        <translation>Foglio di stile XSL</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogEditStylesheet.cpp" line="126"/>
        <source>XSL files (*.xsl)</source>
        <translation>File XSL (*.xsl)</translation>
    </message>
</context>
<context>
    <name>DialogExportPdf</name>
    <message>
        <location filename="../src/widgets/DialogExportPdf.cpp" line="45"/>
        <source>Export PDF</source>
        <translation>Esporta PDF</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogExportPdf.cpp" line="76"/>
        <source>Export to PDF</source>
        <translation>Esporta come PDF</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogExportPdf.cpp" line="78"/>
        <source>PDF documents (*.pdf);;All files (*.*)</source>
        <translation>Documenti PDF (*.pdf);;Tutti i file (*.*)</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogExportPdf.cpp" line="103"/>
        <source>PDF export failed</source>
        <translation>Esportazione come PDF fallita</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogExportPdf.cpp" line="104"/>
        <source>Failed to export PDF to file &apos;%1&apos;</source>
        <translation>Fallita l&apos;esportazione in formato PDF sul file &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>DialogSaveRename</name>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="46"/>
        <source>Save/Rename invoice</source>
        <translation>Salva/rinomina fattura</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="50"/>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="147"/>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="52"/>
        <source>SdI will not accept invoice files with names not following the standard.</source>
        <translation>Per le fatture, l&apos;SdI non accetta nomi di file che non seguono lo standard.</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="69"/>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="149"/>
        <source>Save as...</source>
        <translation>Salva come...</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="87"/>
        <source>Unable to perform the operation</source>
        <translation>Impossibile eseguire l&apos;operazione</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="88"/>
        <source>The file to be deleted does not exist.
Possible causes are:
- the file has already been renamed or deleted;
- access permissions are not sufficient.</source>
        <translation>Il file da cancellare non esiste.
Possibile cause sono:
- il file è già stato rinominato o cancellato;
- i permessi di accesso al file non sono sufficienti.</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="107"/>
        <source>Save/Move invoice</source>
        <translation>Salva/Sposta fattura</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="109"/>
        <source>Invoice documents (*.xml *.p7m);;All files (*.*)</source>
        <translation>Documenti fattura (*.xml *.p7m);;Tutti i file (*.*)</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="119"/>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="137"/>
        <source>Operation failed</source>
        <translation>Operazione fallita</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="120"/>
        <source>Overwrite operation failed.
Check access permission for the destination path.</source>
        <translation>Operazione di sovrascrittura fallita.
Verifica i permessi di accesso per il percorso di destinazione.</translation>
    </message>
    <message>
        <location filename="../src/widgets/DialogSaveRename.cpp" line="137"/>
        <source>Save/Rename operation failed.</source>
        <translation>Operazione di salvataggio/rinomina fallita.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="69"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="81"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="85"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="89"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="101"/>
        <source>Stylesheets</source>
        <translation>Fogli di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="108"/>
        <source>Download stylesheets</source>
        <translation>Scarica fogli di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="135"/>
        <source>Use external OpenSSL executable</source>
        <translation>Usa eseguibe OpenSSL esterno</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="148"/>
        <source>Browse</source>
        <translation>Sfoglia</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="155"/>
        <source>OpenSSL</source>
        <translation>OpenSSL</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="158"/>
        <source>External OpenSSL path</source>
        <translation>Percorso OpenSSL esterno</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="165"/>
        <source>Strings to skip in file names</source>
        <translation>Stringhe da ignorare nei nomi dei file</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="167"/>
        <source>Applies when normalizing the company name.</source>
        <translation>Usate nella normalizzazione dei nomi delle aziende.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="171"/>
        <source>Strings listed here will be skipped when running the normalization of company names for the pattern replacement when saving the invoice or exporting to PDF.
One string per line.
Strings are case sensitive.</source>
        <translation>Le stringhe elencate verranno ignorate durante la normalizzazione dei nomi delle compagnie durante il salvataggio delle fatture o l&apos;esportazione in PDF.
Una stringa per linea.
Nelle stringhe è rispettata la capitalizzazione.</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="190"/>
        <source>Active stylesheet</source>
        <translation>Foglio di stile attivo</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="202"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="230"/>
        <source>Show XML</source>
        <translation>Mostra XML</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="260"/>
        <source>Add stylesheet</source>
        <translation>Aggiungi foglio di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="285"/>
        <source>Edit stylesheet</source>
        <translation>Modifica foglio di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="313"/>
        <source>Are you sure you want to remove the stylesheet &quot;%1&quot; from the list?</source>
        <translation>Sei sicuro di voler cancellare il foglio di stile &quot;%1&quot; dalla lista?</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="316"/>
        <source>Confirm delete of stylesheet</source>
        <translation>Conferma la cancellazione del foglio di stile</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="359"/>
        <source>Select OpenSSL executable</source>
        <translation>Seleziona eseguibile OpenSSL</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="370"/>
        <source>Application restart required</source>
        <translation>L&apos;applicazione richiede riavvio</translation>
    </message>
    <message>
        <location filename="../src/settings/DialogSettings.cpp" line="371"/>
        <source>To load the new language you need to restart the application.</source>
        <translation>Per caricare la nuova lingua è necessario riavviare l&apos;applicazione.</translation>
    </message>
</context>
<context>
    <name>FatturaElettronicaPreview</name>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="94"/>
        <source>Open file</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="100"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="106"/>
        <source>Reload</source>
        <translation>Ricarica</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="113"/>
        <source>Save as...</source>
        <translation>Salva come...</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="120"/>
        <source>Export PDF</source>
        <translation>Esporta PDF</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="127"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="132"/>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="138"/>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="507"/>
        <source>About tools</source>
        <translation>Info strumenti</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="152"/>
        <source>Menu of other actions</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="164"/>
        <source>Main toolbar</source>
        <translation>Pannello dei comandi principale</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="178"/>
        <source>Style: </source>
        <translation>Stile: </translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="210"/>
        <source>Attachments</source>
        <translation>Allegati</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="227"/>
        <source>Fattura Elettronica Preview</source>
        <translation>Anteprima Fattura Elettronica</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="285"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="285"/>
        <source>Error opening the invoice</source>
        <translation>Errore durante l&apos;apertura della fattura</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="345"/>
        <source>Show XML</source>
        <translation>Mostra XML</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="366"/>
        <source>It is possible to use the &quot;Download stylesheets&quot; tool to get them automatically.&lt;br&gt;</source>
        <translation>É possibile usare lo strumento &quot;Scarica fogli di stile&quot; per ottenerli automaticamente.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="369"/>
        <source>No stylesheet configured, please add a stylesheet in the &lt;a href=&quot;#settings-dialog&quot;&gt;settings dialog&lt;/a&gt;.&lt;br&gt;%1This program requires XSL stylesheets for the Fattura Elettronica format to translate the XML in human readable form.</source>
        <translation>Nessun foglio di stile configurato, si prega di aggiungere un foglio di stile nel &lt;a href=&quot;#settings-dialog&quot;&gt;dialogo delle impostazioni&lt;/a&gt;.&lt;br&gt;%1Questo programma richiede fogli di stile XSL per il formato Fattura Elettronica per tradurre l&apos;XML in formato leggibile dall&apos;uomo.</translation>
    </message>
    <message>
        <source>No stylesheet configured, please add a stylesheet in the &lt;a href=&quot;#settings-dialog&quot;&gt;settings dialog&lt;/a&gt;, over there ↗.&lt;br&gt;You can use the &quot;Download stylesheets&quot; tool to get them automatically.&quot;&lt;br&gt;This program requires an XSL stylesheet for the Fattura Elettronica format to translate the XML in human readable form.</source>
        <translation type="vanished">File di style non configurato, è necessario aggiungere un file di stile nel &lt;a href=&quot;#settings-dialog&quot;&gt;dialogo delle impostazioni&lt;/a&gt;, da quella parte ↗.</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="481"/>
        <source>&lt;h2&gt;Tools&lt;/h2&gt;&lt;p&gt;Fattura Elettronica Preview uses the following libraries and resources:&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Qt Framework&lt;/strong&gt;&lt;br&gt;Qt is a full development framework with tools designed to streamline the creation of applications and user interfaces for desktop, embedded, and mobile platforms.&lt;br&gt;License: GNU LGPL version 3 or later&lt;br&gt;Website: &lt;a href=&quot;https://www.qt.io&quot;&gt;https://www.qt.io&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;OpenSSL&lt;/strong&gt;&lt;br&gt;OpenSSL is a robust, commercial-grade, and full-featured toolkit for the TLS and SSL protocols. It is also a general-purpose cryptography library.&lt;br&gt;License: Apache License 2.0&lt;br&gt;Website: &lt;a href=&quot;https://www.openssl.org&quot;&gt;https://www.openssl.org&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;QuaZip&lt;/strong&gt;&lt;br&gt;QuaZip is the C++ wrapper for Gilles Vollant&apos;s ZIP/UNZIP package (AKA Minizip) using Trolltech&apos;s Qt library.&lt;br&gt;License: GNU Lesser General Public License V2.1&lt;br&gt;Website: &lt;a href=&quot;https://github.com/stachenov/quazip&quot;&gt;https://github.com/stachenov/quazip&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Breeze icons theme&lt;/strong&gt;&lt;br&gt;Breeze-icons is a freedesktop.org compatible icon theme. It’s developed by the KDE Community as part of KDE Frameworks 5.&lt;br&gt;License: GNU LGPL version 3 or later&lt;br&gt;Website: &lt;a href=&quot;https://develop.kde.org/frameworks/breeze-icons&quot;&gt;https://develop.kde.org/frameworks/breeze-icons&lt;/a&gt;&lt;/p&gt;&lt;p&gt;The projects may include third-party code that is licensed under specific open-source licenses from the original authors. For more information see the relative website.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Strumenti&lt;/h2&gt;
&lt;p&gt;Fattura Elettronica Preview usa le seguenti librerie e risorse:&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Qt Framework&lt;/strong&gt;&lt;br&gt;Qt è un completo framework di sviluppo con strumenti progettati per semplificare la creazione di applicazioni ed interfacce per desktop, dispositivi integrati e piattaforme mobili.&lt;br&gt;
License: GNU LGPL version 3 or later&lt;br&gt;Website: &lt;a href=&quot;https://www.qt.io&quot;&gt;https://www.qt.io&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;OpenSSL&lt;/strong&gt;&lt;br&gt;OpenSSL è uno strumento robusto, di livello-commerciale, completo per i protocolli TLS e SSL. É anche una libreria di crittografica per uso generico.&lt;br&gt;
License: Apache License 2.0&lt;br&gt;Website: &lt;a href=&quot;https://www.openssl.org&quot;&gt;https://www.openssl.org&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;QuaZip&lt;/strong&gt;&lt;br&gt;QuaZip è un wrapper in C++ per il pacchetto ZIP/UNZIP package (AKA Minizip) di Gilles Vollant che usa la libreria Qt.
&lt;br&gt;License: GNU Lesser General Public License V2.1&lt;br&gt;
Website: &lt;a href=&quot;https://github.com/stachenov/quazip&quot;&gt;https://github.com/stachenov/quazip&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Tema per icone Breeze&lt;/strong&gt;&lt;br&gt;Breeze-icons è un tema per icone compatibile con freedesktop.org. É sviluppato dalla comunità KDE come parte del framework KDE.&lt;br&gt;
License: GNU LGPL version 3 or later&lt;br&gt;
Website: &lt;a href=&quot;https://develop.kde.org/frameworks/breeze-icons&quot;&gt;https://develop.kde.org/frameworks/breeze-icons&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Il progetto può includere codice di terze parti che è sotto qualche licenza open-source indicata dagli autori originali. Per maggiori informazioni si rimanda ai relativi siti web.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>No stylesheet configured, please add a stylesheet in the settings window.
This program requires an XSL stylesheet for the Fattura Elettronica format to function.</source>
        <translation type="vanished">Nessun foglio di stile configurato, è necessario aggiungere un foglio di stile attraverso la finestra delle impostazioni.
Questo programma richiede fogli di stile XSL per il formato Fattura Elettronica per poter funzionare.</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="402"/>
        <source>XML files (*.xml *.p7m)</source>
        <translation>File XML (*.xml *.p7m)</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="460"/>
        <source>&lt;h2&gt;Fattura Elettronica Preview&lt;/h2&gt;&lt;p&gt;Version: %1.%2.%3&lt;/p&gt;&lt;p&gt;Viewer for the Fattura Elettronica format and Sdi messages.&lt;/p&gt;&lt;p&gt;Fattura Elettronica is an XML file format used in Italy to represent invoices. fe-preview displays this file format in human readable form.&lt;/p&gt;&lt;p&gt;This tool does not have editing capabilities and the rendering is done thanks to various style-sheets available online from third sources.&lt;/p&gt;&lt;p&gt;License: GNU GPLv3 or later&lt;/p&gt;&lt;p&gt;Source code repository:&lt;br&gt;&lt;a href=&quot;https://gitlab.com/ariacorrente/fattura-elettronica-preview&quot;&gt;https://gitlab.com/ariacorrente/fattura-elettronica-preview&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Fattura Elettronica Preview&lt;/h2&gt;
&lt;p&gt;Versione: %1.%2.%3&lt;/p&gt;
&lt;p&gt;Visualizzatore per il formato Fattura Elettronica e dei messaggi Sdi.&lt;/p&gt;
&lt;p&gt;Fattura Elettronica è un formato di file XML usato in Italia per rappresentare fatture. fe-preview visualizza questo formato di file in forma leggibile dall&apos;utente.&lt;/p&gt;
&lt;p&gt;Questo strumento non ha capacità di modificare i file e la visualizzazione viene effettuata grazie a vari fogli di stile disponibili online da terze parti.&lt;/p&gt;
&lt;p&gt;License: GNU GPLv3 or later&lt;/p&gt;
&lt;p&gt;Repository del codice sorgente:&lt;br&gt;&lt;a href=&quot;https://gitlab.com/ariacorrente/fattura-elettronica-preview&quot;&gt;https://gitlab.com/ariacorrente/fattura-elettronica-preview&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="525"/>
        <source>Error displaying the file: %1</source>
        <translation>Errore visualizzando il file: %1</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="548"/>
        <source>Save attachment</source>
        <translation>Salva allegato</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="588"/>
        <source>Print failed</source>
        <translation>Stampa fallita</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="589"/>
        <source>Failed to print the requested page.</source>
        <translation>La stampa della pagina è fallita.</translation>
    </message>
    <message>
        <source>Stylesheet &quot;%1&quot; &lt;strong&gt;NOT FOUND&lt;/strong&gt;</source>
        <translation type="vanished">Foglio di stile &quot;%1&quot; &lt;strong&gt;NON TROVATO&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="381"/>
        <source>Stylesheet file not found, please select an existing active stylesheet in the settings window.
This program requires an XSL stylesheet for the Fattura Elettronica format to function.</source>
        <translation></translation>
    </message>
    <message>
        <source>Stylesheet: %1</source>
        <translation type="vanished">Foglio di stile: %1</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="400"/>
        <source>Open Fattura Elettronica XML</source>
        <translation>Apri Fattura Elettronica XML</translation>
    </message>
    <message>
        <source>XML files (*.xml)</source>
        <translation type="vanished">File XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../src/FatturaElettronicaPreview.cpp" line="475"/>
        <source>About Fattura Elettronica Preview</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InvoiceProcessor</name>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="39"/>
        <source>corrupted XML file.</source>
        <translation>file XML corrotto.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="121"/>
        <source>failed to open the temporarly file required to render the XML.</source>
        <translation>fallita l&apos;apertura del file temporaneo necessario per la formattazione del XML.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="189"/>
        <location filename="../src/InvoiceProcessor.cpp" line="658"/>
        <source>failed to open the file.</source>
        <translation>impossibile aprire il file.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="538"/>
        <location filename="../src/InvoiceProcessor.cpp" line="580"/>
        <source>XSL stylesheet not found.</source>
        <translation>foglio di stile XSL non trovato.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="699"/>
        <source>failed to decode P7M format.</source>
        <translation>fallita la decodifica del formato P7M.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="720"/>
        <source>the openssl executable exited with and error.</source>
        <translation>l&apos;eseguibile OpenSSL ha terminato l&apos;operazione con un errore.</translation>
    </message>
    <message>
        <location filename="../src/InvoiceProcessor.cpp" line="732"/>
        <source>failed to launch the openssl executable.</source>
        <translation>fallito il lancio dell&apos;eseguibile OpenSSL.</translation>
    </message>
</context>
<context>
    <name>PatternReplacerWidget</name>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="53"/>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="57"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="69"/>
        <source>Pattern:</source>
        <translation>Pattern:</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="70"/>
        <source>Invoice number:</source>
        <translation>Numero fattura:</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="71"/>
        <source>Original path:</source>
        <translation>Percorso originale:</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="72"/>
        <source>Destination path:</source>
        <translation>Percorso destinazione:</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="131"/>
        <source>Help on replacement labels</source>
        <translation>Aiuto sulle etichette di sostituzione</translation>
    </message>
    <message>
        <location filename="../src/widgets/PatternReplacerWidget.cpp" line="132"/>
        <source>&lt;p&gt;The &apos;Destination path&apos; is calculated starting from the &apos;Pattern&apos; and replacing the labels starting with &apos;%&apos; with the corresponding data.&lt;/p&gt;
&lt;p&gt;Labels used in the pattern:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;%F: folder of source file;&lt;/li&gt;&lt;li&gt;%f: base name of the file, without folder and extension;&lt;/li&gt;&lt;li&gt;%x: extension of source file (xml or xml.p7m);&lt;/li&gt;&lt;li&gt;%d: complete date of invoice&lt;br/&gt;    For the elements of the date:    &lt;ul&gt;        &lt;li&gt;%D: day;&lt;/li&gt;        &lt;li&gt;%M: month;&lt;/li&gt;        &lt;li&gt;%Y: year;&lt;/li&gt;    &lt;/ul&gt;&lt;/li&gt;&lt;li&gt;%n: number of invoice;&lt;/li&gt;&lt;li&gt;%a: name of cedente/prestatore;&lt;/li&gt;&lt;li&gt;%A: normalized name of cedente/prestatore;&lt;/li&gt;&lt;li&gt;%b: name of cessionario/committente;&lt;/li&gt;&lt;li&gt;%B: normalized name of cessionario/committente;&lt;/li&gt;&lt;li&gt;%s: path separator.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;If the opened file is a collection of invoices you may need to select the exact invoice you want to read data from.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Il &apos;Percordo destinazione&apos; è calcolato partendo dal &apos;Pattern&apos; e rimpiazzando le etichette che iniziano con &apos;%&apos; con i dati corrispondenti&lt;/p&gt;
&lt;p&gt;Etichette usate nel pattern:&lt;/p&gt;
&lt;ul&gt;
    &lt;li&gt;%F: cartella del file di partenza;&lt;/li&gt;
    &lt;li&gt;%f: nome base del file di partenza, senza cartella ed estensione;&lt;/li&gt;
    &lt;li&gt;%x: estensione del file di partenza (xml o xml.p7m);&lt;/li&gt;
    &lt;li&gt;%d: data completa della fattura&lt;br/&gt;
    Per gli elementi della data:
    &lt;ul&gt;        
        &lt;li&gt;%D: giorno;&lt;/li&gt;        
        &lt;li&gt;%M: mese;&lt;/li&gt;        
        &lt;li&gt;%Y: anno;&lt;/li&gt;    
    &lt;/ul&gt;
    &lt;/li&gt;
    &lt;li&gt;%n: numero della fattura;&lt;/li&gt;
    &lt;li&gt;%a: nome del cedente/prestatore;&lt;/li&gt;
    &lt;li&gt;%A: nome normalizzato del cedente/prestatore;&lt;/li&gt;
    &lt;li&gt;%b: nome del cessionario/committente;&lt;/li&gt;
    &lt;li&gt;%B: nome normalizzato del cessionario/committente;&lt;/li&gt;
    &lt;li&gt;%s: separatore del percorso del filesystem.&lt;/li&gt;
    &lt;/ul&gt;
    &lt;p&gt;Se il file aperto è una collezione di fatture è possibile sia necessario selezionare l&apos;esatta fattura da cui leggere i dati.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
