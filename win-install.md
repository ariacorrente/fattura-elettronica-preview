WINDOWS INSTALL NOTES
=====================

Notes about how to copile and install fe-preview in a MS Windows system.

This was tested under windows 10 in date 28/04/2024. The process will probably
work with older versions but this is what I have done and found working.

## Requirements

- **Qt6** at version 6.6.3 (this version fixed a bug in the XML module) or
    newer. Through the Qt installer it is recommended to install also:

    - **QtCreator** IDE is optional but allows to easily handle the build
        environment in windows.

    - **OpenSSL** (version 3.something) library to open p7m invoices without an
        external openSSL executable

- **MSVC 2019** 64bit version, required because it is the only compiler
    supported by the Qt module WebView. The easier way to install the compiler
    is installing the IDE Visual Studio 2019.

- **quazip** library (tested with version 1.4), required by the automatic XSL
    stylesheet installer dialog.

- **zlib** library (tested with version 1.3.1), required by the quazip library.

## Compile

For all the builds remember to:

- set CMAKE_INSTALL_PREFIX to a "install" folder near the "build" folder. By
    default it installs in a system folder and requires the admin password.

- set the CMAKE_RELASE_TYPE to "Release"

- the MSVC compiler should be visible in the environemnt. Use QtCreator or a
    console with the correct environment, for example the ones pre-configured by
    Qt or Visual Studio.

### Compile zlib

1. download the zlib sources (https://zlib.net) and extract

3. build the target "zlib" (in QtCreator use the button "build project", not
    the button "Run"). If everything is ok you can run the "install" target to
    build the required files in the "install" folder

### Compile quazip

1. clone the git repo of quazip (https://github.com/stachenov/quazip)

2. configure the build environment:

    - disable QUAZIP_BZIP2 option, it complicates things and is not required
        for fe-preview

    - add to CMAKE_PREFIX_PATH:

        - the Qt "msvc2019_64" folder

        - the "install" folder of zlib

3. build the "QuaZip" target. If everything is ok run "install" target to
    populate the "install" folder.

### Compile fe-preview

1. Clone the git repo (https://gitlab.com/ariacorrente/fattura-elettronica-preview)

2. Configure the build environment:

    - add to CMAKE_PREFIX_PATH:

        - the Qt "msvc2019_64" folder

        - the "install" folder of zlib

        - the "install" folder of QuaZip

        - the "Win_x64" folder of OpenSSL

    - enable the "ENABLE_DOWNLOAD_TOOL" option

3. build the "lrelease" target for the localization

4. in theory you can build the "icons" target to generate the icons but it needs
    to find the "inkscape" and "convert" (from ImageMagick) programs. It is
    probably easier to copy the "png" and "ico" images from a linux build, an
    older build or install, the "IFW-template" folder.

5. build and run the "fe-preview" target and check all the features are working

6. build the "install" target to generate the folder to be deployed. This will copy:

    - the fe-preview executable and all the project resources (icons, xsl, and localization)

    - all the Qt libraries and other stuff that the windeployqt tool decides are required

7. You need to manually copy in the install directory the following dll
    libraries from the respective install folder:

    - OpenSSL: "libcrypto-3-x64.dll" "libssl-3-x64.dll"

    - QuaZip: "quazip1-qt6.dll"

    - Zlib: "zlib.dll"

    or similar names depending on the version in use.

This "install" directory can be manually installed in the target system and
should allow to execute the fe-preview.exe program with all features enabled.

### Build the installer (optional)

It is possible to build an installer with the "Qt Installer FrameWork". The
directory "IFW-template" contains the configuration and the data required by IFW
to build the installer. This is the procedure:

1. Configure the project with cmake. This will copy and compile the files from
    the source directory to the build directory of cmake (icons, license and
    XML configurations).

2. replace the
    "${CMAKE_BINARY_DIR}\IFW-template\packages\com.dominiofelice.fepreview\data\data.7z"
    with the compressed files present in the "install" folder generated in the
    previous chapter. The compressed packaged must contain directly the files
    WITHOUT a containing folder.

    It is possible to use the Qt tool archivegen to create the compressed
    package in 7z format and save some MB:

        C:\Qt\Tools\QtInstallerFramework\4.7\bin\archivegen.exe
            fe-preview_package
            install\*

3. build the installer with a command like this:

        C:\Qt\Tools\QtInstallerFramework\4.7\bin\binarycreator.exe
            --offline-only
            -c build\IFW-template\config\config.xml
            -p build\IFW-template\packages
            fe-preview_installer_v0.6.exe

    The directory to use is the one in the CMAKE_BINARY_DIR.
