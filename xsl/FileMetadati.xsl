<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msg="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fattura/messaggi/v1.0">

  <!-- FILE METADATI -->
  <xsl:template name="fileMetadati">
    <xsl:param name="metadati" />
    <header>
        FileMetadati versione <xsl:value-of select="$metadati/@versione"/>
    </header>
    <div class="content">

      <span class="entryLabel">Identificativo Sdi:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/IdentificativoSdI"/></span>

      <span class="entryLabel">Nome file:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/NomeFile"/></span>

      <span class="entryLabel">Hash:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/Hash"/></span>

      <span class="entryLabel">Codice destinatario:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/CodiceDestinatario"/></span>

      <span class="entryLabel">Formato:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/Formato"/>
        <xsl:choose>
          <xsl:when test="$metadati/Formato='FPR12'"> (Fattura verso privati)</xsl:when>
          <xsl:when test="$metadati/Formato='FPA12'"> (Fattura verso PA)</xsl:when>
          <xsl:otherwise>(Formato non standard)</xsl:otherwise>
        </xsl:choose>
      </span>

      <span class="entryLabel">Tentativi invio:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/TentativiInvio"/></span>

      <span class="entryLabel">Message Id:</span>
      <span class="entryValue"><xsl:value-of select="$metadati/MessageId"/></span>

      <xsl:if test="Note">
        <span class="entryLabel">Note:</span>
        <span class="entryValue"><xsl:value-of select="$metadati/Note"/></span>
      </xsl:if>

    </div>
  </xsl:template>

  <!-- ROOT -->
  <xsl:template match="/">
    <html>

      <head>
        <style>
          .content {
            display: grid;
            grid-template-columns: max-content auto;
            grid-auto-columns: min-content;
            border: 1px solid black;
            box-shadow: -4px 0 4px gray;
            column-gap: 6px;
            overflow: hidden;
          }
          .content>* {
            margin-left: 6px;
            border-right: 0px;
          }
          .entryValue {
            font-weight: bold;
            overflow-x: auto;
          }
          header {
            position: absolute;
            top: 0;
            right: 0;
            width: 100%;
            text-align: right;
            font-size: 12px;
            padding: 6px;
            color: gray;
          }
        </style>
      </head>

      <body>
        <h1>File Metadati</h1>
        <p>File di metadati che SdI invia al destinatario, insieme al file FatturaPA.</p>

        <xsl:call-template name="fileMetadati">
          <xsl:with-param name="metadati" select="msg:FileMetadati"/>
        </xsl:call-template>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
