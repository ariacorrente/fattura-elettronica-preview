<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msg="http://www.fatturapa.gov.it/sdi/messaggi/v1.0"
  xmlns:ds="http://www.w3.org/2000/09/xmldsig#">

  <!-- RIFERIMENTO FATTURA -->
  <xsl:template name="riferimentoFattura">
    <xsl:param name="riferimento"/>

    <div class="content">
      <span class="entryLabel">Numero fattura:</span>
      <span class="entryValue"><xsl:value-of select="$riferimento/NumeroFattura"/></span>

      <span class="entryLabel">Anno fattura:</span>
      <span class="entryValue"><xsl:value-of select="$riferimento/AnnoFattura"/></span>

      <xsl:if test="PosizioneFattura">
        <span class="entryLabel">Posizione fattura:</span>
        <span class="entryValue"><xsl:value-of select="$riferimento/PosizioneFattura"/></span>
      </xsl:if>
    </div>

  </xsl:template>

  <!-- ESITO COMMITTENTE -->
  <xsl:template name="esitoCommittente">
    <xsl:param name="esito"/>
    <header>
      FileMetadati versione <xsl:value-of select="$esito/@versione"/><br/>
      <xsl:if test="$esito/@IntermediarioConDupliceRuolo">
        Intermediario con duplice ruolo
      </xsl:if>
    </header>

    <div class="content">

      <span class="entryLabel">Identificativo Sdi:</span>
      <span class="entryValue"><xsl:value-of select="$esito/IdentificativoSdI"/></span>

      <xsl:if test="$esito/RiferimentoFattura">
        <span class="entryLabel">Riferimento fattura:</span>
        <xsl:call-template name="riferimentoFattura">
          <xsl:with-param name="riferimento" select="$esito/RiferimentoFattura"/>
        </xsl:call-template>
      </xsl:if>

      <span class="entryLabel">Esito:</span>
      <span class="entryValue"><xsl:value-of select="$esito/Esito"/>
        <xsl:choose>
          <xsl:when test="$esito/Esito='EC01'"> (<span class="positive">Accettazione</span>)</xsl:when>
          <xsl:when test="$esito/Esito='EC02'"> (<span class="negative">Rifiuto</span>)</xsl:when>
          <xsl:otherwise> (Formato non standard)</xsl:otherwise>
        </xsl:choose>
      </span>

      <xsl:if test="Descrizione">
        <span class="entryLabel">Descrizione:</span>
        <span class="entryValue"><xsl:value-of select="$esito/Descrizione"/></span>
      </xsl:if>

      <xsl:if test="MessageIdCommittente">
        <span class="entryLabel">Message Id committente:</span>
        <span class="entryValue"><xsl:value-of select="$esito/MessageIdCommittente"/></span>
      </xsl:if>

      <xsl:if test="ds:Signature">
        <span class="entryLabel">Signature:</span>
        <span class="entryValue"><xsl:value-of select="$esito/ds:Signature"/></span>
      </xsl:if>
    </div>
  </xsl:template>

  <!-- NOTIFICA ESITO -->
  <xsl:template name="notificaEsito">
    <xsl:param name="notifica" />
    <div class="content">

      <span class="entryLabel">Identificativo Sdi:</span>
      <span class="entryValue"><xsl:value-of select="$notifica/IdentificativoSdI"/></span>

      <span class="entryLabel">Nome file:</span>
      <span class="entryValue"><xsl:value-of select="$notifica/NomeFile"/></span>

      <span class="entryLabel">Esito committente:</span>
      <xsl:call-template name="esitoCommittente">
        <xsl:with-param name="esito" select="$notifica/EsitoCommittente"/>
      </xsl:call-template>

      <span class="entryLabel">Message Id:</span>
      <span class="entryValue"><xsl:value-of select="$notifica/MessageId"/></span>

      <xsl:if test="PecMessageId">
        <span class="entryLabel">PEC Message Id:</span>
        <span class="entryValue"><xsl:value-of select="$notifica/PecMessageId"/></span>
      </xsl:if>

      <xsl:if test="Note">
        <span class="entryLabel">Note:</span>
        <span class="entryValue"><xsl:value-of select="$notifica/Note"/></span>
      </xsl:if>

      <xsl:if test="$notifica/ds:Signature">
        <span class="entryLabel">Firma:</span>
        <span class="entryValue">Il documento è firmato.</span>
      </xsl:if>
    </div>

  </xsl:template>

  <!-- ROOT -->
  <xsl:template match="/">
    <html>

      <head>
        <style>
          .content {
            display: grid;
            grid-template-columns: max-content auto;
            grid-auto-columns: min-content;
            border: 1px solid black;
            box-shadow: -4px 0 4px gray;
            column-gap: 6px;
            overflow: hidden;
          }
          .content>* {
            margin-left: 6px;
            border-right: 0px;
          }
          .entryValue {
            font-weight: bold;
            overflow-x: auto;
          }
          header {
            position: absolute;
            top: 0;
            right: 0;
            width: 100%;
            text-align: right;
            font-size: 12px;
            padding: 6px;
            color: gray;
          }
          .positive {
            color: green;
          }
          .negative {
            color: red;
          }
        </style>
      </head>

      <body>
        <h1>Notifica Esito</h1>
        <p>Messaggio con il quale il SdI inoltra al trasmittente la notifica di esito committente eventualmente ricevuta dal destinatario della fattura.</p>

        <xsl:call-template name="notificaEsito">
          <xsl:with-param name="notifica" select="msg:NotificaEsito"/>
        </xsl:call-template>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
