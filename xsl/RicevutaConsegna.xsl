<?xml version="1.0" encoding="UTF-8"?>
<!--
  Sdi have two similar XSD for the messages, one for PA and one for B2B.
  "RicevutaConsegna" message type is in common, apart some small differences.
  This XSL works for both types, the two namespace are:
  - xmlns::pa is the namespace for messages about PA invoices
  - xmlns::b2b is the namespace for messages bout B2B invoices
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:pa="http://www.fatturapa.gov.it/sdi/messaggi/v1.0"
    xmlns:b2b="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fattura/messaggi/v1.0"
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#">

    <!-- DESTINATARIO -->
    <xsl:template name="destinatario">
      <xsl:param name="destinatario"/>
        <div class="content">
          <span class="entryLabel">Codice:</span>
          <span class="entryValue"><xsl:value-of select="$destinatario/Codice"/></span>

          <span class="entryLabel">Descrizione:</span>
          <span class="entryValue"><xsl:value-of select="$destinatario/Descrizione"/></span>
        </div>
    </xsl:template>

    <!-- DATA E ORA -->
    <xsl:template name="templateData">
      <xsl:param name="data" />

      <xsl:variable name="year">
        <xsl:value-of select="substring($data,1,4)" />
      </xsl:variable>

      <xsl:variable name="month">
        <xsl:value-of select="substring($data,6,2)" />
      </xsl:variable>

      <xsl:variable name="day">
        <xsl:value-of select="substring($data,9,2)" />
      </xsl:variable>

      <xsl:variable name="time">
        <xsl:value-of select="substring($data,12,5)" />
      </xsl:variable>

      <xsl:variable name="seconds">
        <xsl:value-of select="substring($data,18,2)" />
      </xsl:variable>

      <xsl:variable name="millisecond">
        <xsl:value-of select="substring($data,21,3)" />
      </xsl:variable>

      <xsl:variable name="utc">
        <xsl:value-of select="substring($data,24,6)" />
      </xsl:variable>

      <span class="entryValue">
        <xsl:value-of select="$day" />/<xsl:value-of select="$month" />/<xsl:value-of select="$year" />
        alle <xsl:value-of select="$time" />, <xsl:value-of select="$seconds" />s e <xsl:value-of select="$millisecond" />ms
        UTC<xsl:value-of select="$utc" />
      </span>
    </xsl:template>

    <!-- RICEVUTA CONSEGNA -->
    <xsl:template name="ricevuta-consegna">
        <xsl:param name="ricevuta" />
        <header>
          RicevutaConsegna versione <xsl:value-of select="$ricevuta/@versione"/>

          <!-- Only for PA -->
          <xsl:if test="$ricevuta/@IntermediarioConDupliceRuolo">
            Intermediario con duplice ruolo
          </xsl:if>

          <!-- Only for B2B -->
          <xsl:if test="$ricevuta/@FlussoSemplificato">
            Flusso semplificato
          </xsl:if>
        </header>
        <div class="content">
          <span class="entryLabel">Identificativo SdI:</span>
          <span class="entryValue"><xsl:value-of select="$ricevuta/IdentificativoSdI"/></span>

          <span class="entryLabel">Nome file:</span>
          <span class="entryValue"><xsl:value-of select="$ricevuta/NomeFile"/></span>

          <!-- Only for B2B -->
          <xsl:if test="$ricevuta/Hash">
            <span class="entryLabel">Hash:</span>
            <span class="entryValue"><xsl:value-of select="$ricevuta/Hash"/></span>
          </xsl:if>

          <span class="entryLabel">Data/Ora Ricezione:</span>
          <xsl:call-template name="templateData">
            <xsl:with-param name="data" select="$ricevuta/DataOraRicezione"/>
          </xsl:call-template>

          <span class="entryLabel">Data/Ora Consegna:</span>
          <xsl:call-template name="templateData">
            <xsl:with-param name="data" select="$ricevuta/DataOraConsegna"/>
          </xsl:call-template>

          <span class="entryLabel">Destinatario:</span>
          <xsl:call-template name="destinatario">
            <xsl:with-param name="destinatario" select="$ricevuta/Destinatario"/>
          </xsl:call-template>

          <xsl:if test="$ricevuta/RiferimentoArchivio">
            <span class="entryLabel">Riferimento Archivio:</span>
            <div class="content">
              <span class="entryLabel">Identificativo SdI:</span>
              <span class="entryValue"><xsl:value-of select="$ricevuta/RiferimentoArchivio/IdentificativoSdI"/></span>

              <span class="entryLabel">Nome File:</span>
              <span class="entryValue"><xsl:value-of select="$ricevuta/RiferimentoArchivio/NomeFile"/></span>
            </div>
          </xsl:if>

          <span class="entryLabel">Message Id:</span>
          <span class="entryValue"><xsl:value-of select="$ricevuta/MessageId"/></span>

          <xsl:if test="$ricevuta/PecMessageId">
            <span class="entryLabel">PEC Message Id:</span>
            <span class="entryValue"><xsl:value-of select="$ricevuta/PecMessageId"/></span>
          </xsl:if>

          <xsl:if test="$ricevuta/Note">
            <span class="entryLabel">Note:</span>
            <span class="entryValue"><xsl:value-of select="$ricevuta/Note"/></span>
          </xsl:if>

          <xsl:if test="$ricevuta/ds:Signature">
            <span class="entryLabel">Firma:</span>
            <span class="entryValue">Il documento è firmato.</span>
          </xsl:if>

        </div>
    </xsl:template>

    <!-- ROOT -->
    <xsl:template match="/">

      <html>

      <head>
        <style>
          .content {
            display: grid;
            grid-template-columns: max-content auto;
            grid-auto-columns: min-content;
            border: 1px solid black;
            box-shadow: -4px 0 4px gray;
            column-gap: 6px;
            overflow: hidden;
          }
          .content>* {
            margin-left: 6px;
            border-right: 0px;
          }
          .entryValue {
            font-weight: bold;
            overflow-x: auto;
          }
          header {
            position: absolute;
            top: 0;
            right: 0;
            width: 100%;
            text-align: right;
            font-size: 12px;
            padding: 6px;
            color: gray;
          }
        </style>
      </head>

      <body>
        <h1>Ricevuta di consegna</h1>
        <p>Messaggio che SdI invia al trasmittente per certificare l’avvenuta consegna al destinatario del file FatturaPA.</p>

        <!-- Only for B2B -->
        <xsl:if test="b2b:RicevutaConsegna">
          <p>Il messaggio riguarda una fattura da/verso una azienda.</p>
          <xsl:call-template name="ricevuta-consegna">
              <xsl:with-param name="ricevuta" select="b2b:RicevutaConsegna"/>
          </xsl:call-template>
        </xsl:if>

        <!-- Only for PA -->
        <xsl:if test="pa:RicevutaConsegna">
        <p>Il messaggio riguarda una fattura da/verso una pubblica amministrazione.</p>
          <xsl:call-template name="ricevuta-consegna">
              <xsl:with-param name="ricevuta" select="pa:RicevutaConsegna"/>
          </xsl:call-template>
        </xsl:if>

      </body>
      </html>
    </xsl:template>

</xsl:stylesheet>
