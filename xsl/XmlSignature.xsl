<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ns3="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fattura/messaggi/v1.0"
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#">

    <xsl:template name="templateSignature">
        <xsl:param name="signature" />
        <span class="entryLabel">Signature <strong>(da finire)</strong>:</span>

        <div class="content">
        <span class="entryLabel">Id:</span>
        <span class="entryValue"><xsl:value-of select="$signature/@Id"/></span>

        <span class="entryLabel">Signed Info:</span>
        <div class="content">
            <span class="entryLabel">Canonicalization Method:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:SignedInfo/ds:CanonicalizationMethod /@Algorithm"/></span>

            <span class="entryLabel">Signature Method:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:SignedInfo/ds:SignatureMethod/@Algorithm"/></span>

            <xsl:for-each select="$signature/ds:SignedInfo/ds:Reference">
                <xsl:call-template name="signature-signatureInfo-reference">
                    <xsl:with-param name="reference" select="." />
                </xsl:call-template>
            </xsl:for-each>
        </div>

        <!-- Signature Value -->
        <span class="entryLabel">Signature Value</span>
        <div class="content">
            <span class="entryLabel">Id:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:SignatureValue/@Id"/></span>

            <span class="entryLabel">Value:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:SignatureValue" /></span>
        </div>

        <!-- Key Info -->
        <span class="entryLabel">Key Info:</span>
        <div class="content">
            <span class="entryLabel">Id:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:KeyInfo/@Id"/></span>

            <!-- Semplificare strutture vuote? -->
            <span class="entryLabel">X509Data/X509Certificate:</span>
            <span class="entryValue"><xsl:value-of select="$signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate" /></span>
        </div>

        <!-- Object -->
        <span class="entryLabel">Object:</span>
        <div class="content">
            <span class="entryValue"><xsl:value-of select="$signature/ds:Object" /></span>
        </div>

        </div>
    </xsl:template>


    <xsl:template name="signature-signatureInfo-reference">
        <xsl:param name="reference" />
        <span class="entryLabel">Reference #<xsl:value-of select="position()"/>:</span>
        <div class="content">
            <span class="entryLabel">Id:</span>
            <span class="entryValue"><xsl:value-of select="$reference/@Id"/></span>

            <span class="entryLabel">URI:</span>
            <span class="entryValue"><xsl:value-of select="$reference/@URI"/></span>

            <span class="entryLabel">Type:</span>
            <span class="entryValue"><xsl:value-of select="$reference/@Type"/></span>

            <span class="entryLabel">Digest Method Algorithm:</span>
            <span class="entryValue"><xsl:value-of select="$reference/ds:DigestMethod/@Algorithm"/></span>

            <span class="entryLabel">Digest value:</span>
            <span class="entryValue"><xsl:value-of select="$reference/ds:DigestValue"/></span>

            <xsl:for-each select="$reference/ds:Transforms/ds:Transform">
                <xsl:call-template name="signature-signatureInfo-reference-transform">
                    <xsl:with-param name="transform" select="." />
                </xsl:call-template>
            </xsl:for-each>

        </div>
    </xsl:template>

    <xsl:template name="signature-signatureInfo-reference-transform">
        <xsl:param name="transform" />
        <span class="entryLabel">Transform:</span>
        <div class="content">
            <span class="entryLabel">Algorithm:</span>
            <span class="entryValue"><xsl:value-of select="$transform/@Algorithm"/></span>

            <!-- XPATH non va e non so perchè -->
            <span class="entryLabel">XPath:</span>
            <div class="content">

                <span class="entryLabel">Value:</span>
                <span class="entryValue"><xsl:value-of select="$transform/XPath"/></span>


                <span class="entryLabel">xmlns:</span>
                <span class="entryValue"><xsl:value-of select="$transform/XPath/@xmlns"/></span>

                <span class="entryLabel">Filter:</span>
                <span class="entryValue"><xsl:value-of select="$transform/XPath/@Filter"/></span>
            </div>
        </div>
    </xsl:template>

        <xsl:template match="/">

        <html>

        <head>
          <style>
            .content {
              display: grid;
              grid-template-columns: max-content auto;
              grid-auto-columns: min-content;
              border: 1px solid black;
              box-shadow: -4px 0 4px gray;
              column-gap: 6px;
              overflow: hidden;
            }
            .content>* {
              margin-left: 6px;
            }
            .entryValue {
              font-weight: bold;
              overflow-x: auto;
            }
            header {
              position: absolute;
              top: 0;
              right: 0;
              width: 100%;
              text-align: right;
              font-size: 12px;
              padding: 6px;
              color: gray;
            }
          </style>
        </head>

        <body>
            <h1>XML Signature</h1>
            <p>Test only.</p>

            <xsl:call-template name="templateSignature">
                <xsl:with-param name="signature" select="ds:Signature"/>
            </xsl:call-template>

        </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
